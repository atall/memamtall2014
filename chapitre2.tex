\chapter{Arithmétique des polyominos}

Dans ce chapitre, nous nous intéressons à une classe de morphismes appelés \emph{morphismes homologues} pour définir la notion de polyominos premiers et composés. Nous étudions par la suite les aspects algorithmiques des ces objets.

\section{Polyominos parallélogrammes }

Dans \cite{bn}, $\textbf{D. Beauquier}$ et $\textbf{M. Nivat}$ démontrent qu'un polyomino $P$ pave un plan par translation si et seulement s'il admet un mot de contour
$$w=xyz\widehat{x}\widehat{y}\widehat{z},$$
avec au plus un mot vide $\epsilon$ parmi $x$, $y$ ou $z$ et où l'antimorphisme $\enspace \widehat{} \enspace$ est défini au chapitre précédent (page \pageref{antimorphismeeeee}). La factorisation ($xyz\widehat{x}\widehat{y}\widehat{z}$) est appelée une \emph{BN-factorisation} de $P$ et, par abus de notation, on écrit simplement $xyz\widehat{x}\widehat{y}\widehat{z}$. Si $x$, $y$ ou $z$ est un mot vide alors $P$ est un polyomino
parallélogramme (\textbf{tuile carrée} dans \cite{PSC2006h}). Autrement, on parle de
polyomino hexagone (\textbf{tuile hexagone} dans \cite{PSC2006h}). Il est à noter
que les \textbf{polyominos parallélogrammes} (voir figure~\ref{polypara}) ont été introduits, il y a plus de 40 ans, avec un sens totalement différent  de celui utilisé
dans le mémoire  \cite{polya}. Néanmoins, nous préférons garder la terminologie
\textbf{parallélogramme}  puisque qu'elle semble appropriée.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.9]{Logos/parallelogrampoly}
\end{center}

\caption[Un polyomino parallélogramme et le pavage qu'il induit (le mot
de contour admet une BN-factorisation $\0\0\1\0\cdot  \1\0\1\2\1\1\cdot \2\3\2\2\cdot \3\3\0\3\2\3$)]{(a) Un polyomino parallélogramme et le pavage qu'il induit (le mot
de contour admet une BN-factorisation $\0\0\1\0\cdot  \1\0\1\2\1\1\cdot \2\3\2\2\cdot \3\3\0\3\2\3$).
Les points noirs indiquent les points de factorisation du mot de contour
et forment clairement un parallélogramme. (b) Un polyomino hexagone
ayant comme mot de contour $\0\0\0\cdot  \0\0\1\0\cdot \1\1\2\1\cdot  \2\2\2\cdot  \2\3\2\2\cdot  \3\0\3\3$
et le pavage correspondant.}
\label{polypara}
\end{figure}



\begin{prop}
\cite{brlek2005note} Le nombre d'enroulements d'un mot de contour $w$ est $\mathcal{T}([w])\pm 1$.
\label{enroul} 
\end{prop}
Une notion particulièrement utile est celle d'\emph{orientation de chemin}. On dit que $w$ est \emph{positivement orienté} \index{positivement orienté} si son nombre d'enroulement est $\mathcal{T}([w]) = 1$ (autrement dit, on le parcourt dans le sens anti-horaire). En général, si $XY \widehat{X}\widehat{Y}$ est un chemin (simple ou complexe), son nombre d'enroulement peut être calculé à partir de sa BN-factorisation par la formule:
$$\mathcal{T}([XY \widehat{X}\widehat{Y}])= \mathcal{T} (X,Y ) + \mathcal{T} (Y,\widehat{X} ) + \mathcal{T} (\widehat{X},\widehat{Y} ) + \mathcal{T} (\widehat{Y},X).$$


Par conséquent, chaque tuile carrée satisfait la condition suivante:
\begin{prop}
Soit $ w=XY \widehat{X}\widehat{Y}$ le mot de contour d'une tuile
carrée alors:
\begin{center}
 $\Delta (X,Y ) = \Delta (Y,\widehat{X} ) =\Delta (\widehat{X},\widehat{Y} ) = \Delta (\widehat{Y},X)=\alpha$,
\end{center}
\label{tuilecarr}
\end{prop}
où le parcours est orienté positivement si $\alpha = \1$ et négativement si $\alpha = \3$.

\section{Polyominos premiers et composés }
\begin{defn}
\cite{masse2014arithmetics} Un morphisme $\varphi : \Freeman^* \rightarrow \Freeman^*$ est appelé
\emph{homologue} si $\varphi(a) = \hat{\varphi(\bar{a})}$ pour tout
$a \in \Freeman$.
\end{defn}

Autrement dit, les morphismes homologues remplacent les deux pas élémentaires horizontaux et verticaux par des chemins arbitraires. En effet, pour l'étape horizontale, $\varphi(\0)$ est traversé en direction contraire par rapport à  $\varphi(\2)$. La même idée s'applique pour l'étape verticale. En effet, $\varphi(\1)$ est traversé en direction contraire par rapport à $\varphi(\3)$.

\begin{example}
Considérons le morphisme $\varphi : \Freeman^* \rightarrow \Freeman^*$ défini par
$$\varphi(\0) = \0 \1 \0 \0 \0, \quad \varphi(\1) = \1 \2 \1, \quad \varphi(\2) = \2 \2 \2 \3 \2 \quad \mbox{et} \quad \varphi(\3) = \3 \0 \3.$$
On a
\begin{eqnarray*}
  \widehat{\varphi(\0)} & = & \widehat{\0\1\0\0\0} = \2\2\2\3\2 = \varphi(\2), \\
  \widehat{\varphi(\1)} & = & \widehat{\1\2\1} = \3\0\3 = \varphi(\3),
\end{eqnarray*}
de sorte que $\varphi$ est un morphisme homologue.
\end{example}

Une conséquence immédiate de la définition est que tout morphisme homologue commute avec l'antimorphisme~$\widehat{~}$~:
\begin{prop}
Pour tout morphisme homologue $\varphi$ et pour tout $w \in \Freeman^*$, $\widehat{\varphi(w)} = \varphi(\widehat{w})$.
\end{prop}
\begin{proof}
Soit $n = |w|$ et $w = w_0w_1\cdots w_n$. Nous avons 
\begin{eqnarray*}
  \widehat{\varphi(w)}
    & = & \widehat{\varphi(w_0w_1\cdots w_{n-1}w_n)} \\
    & = & \widehat{\varphi(w_0)\varphi(w_1)\cdots \varphi(w_{n-1})\varphi(w_n)} \\
    & = & \widehat{\varphi(w_n)}\widehat{\varphi(w_{n-1})} \cdots \widehat{\varphi(w_2)} \widehat{\varphi(w_1)} \quad \mbox{(propriété d'un antimorphisme)} \\
    & = & \varphi(\overline{w_n})\varphi(\overline{w_{n-1}}) \cdots \varphi(\overline{w_2}) \varphi(\overline{w_1}) \quad \mbox{(par définition de morphisme homologue)} \\
    & = & \varphi(\overline{w_nw_{n-1}\cdots w_2w_1}) \\
    & = & \varphi(\widehat{w}),
\end{eqnarray*}
tel que voulu.
\end{proof}

Comme les polyominos sans trou ont un mot de contour simple, une condition additionnelle des morphismes homologues est nécessaire afin de définir les polyominos sans trou premiers et composés.

\begin{defn}
Soit $\varphi$  un morphisme homologue. Nous disons que $\varphi$ est un \emph{morphisme parallélogramme} si:
\begin{enumerate}
  \item $\varphi(\0\1\2\3)$ est un mot de contour, c'est-à-dire un chemin fermé et auto-évitant;
  \item $\First{\varphi(a)} = a$ pour tout $a \in \Freeman$. 
\end{enumerate}
\end{defn}

\begin{example}
Le morphisme $\varphi$ défini par
$$\varphi(\0) = \0\0, \quad \varphi(\1) = \1\2\1, \quad \varphi(\2) = \2\2 \quad \mbox{et}\quad  \varphi(\3) = \3\0\3$$
est un morphisme parallélogramme. En effet, le chemin $\varphi(\0\1\2\3)$ est bien un mot de contour (voir figure~\ref{tuilparall}).
\begin{figure}[tbph]
  \centering
  \includegraphics[scale=0.7]{Logos/tuileA}
  \caption{La tuile induite par le morphisme parallélogramme $\varphi$ défini par $0 \mapsto \0\0, \1 \mapsto \1\2\1, \2 \mapsto \2\2, \3 \mapsto \3\0\3$.}
  \label{tuilparall}
\end{figure}
\end{example}

La définition de morphisme parallélogramme induit des contraintes sur les premières et dernières lettres de la façon suivante~:
\begin{prop} \label{morphismpara}
Soit $\varphi$ un morphisme parallélogramme. Alors
\begin{enumerate}
  \item $\First{\varphi(\0)} = \Last{\varphi(\0)}$ et $\First{\varphi(\1)} = \Last{\varphi(\1)}$;
  \item Les premières lettres sont mutuellement distinctes, c'est-à-dire
    $$\{\First{\varphi(\0)}, \First{\varphi(\1)}, \First{\varphi(\2)}, \First{\varphi(\3)}\} = \{\0, \1, \2, \3\}.$$
\end{enumerate}
\end{prop}

\begin{proof}
Notons que $\varphi(\0\1\2\3)$ décrit une tuile parallélogramme dans le sens positif. Par la proposition~\ref{tuilecarr}, nous avons donc $\Last{\varphi(\0)}+\1=\First{\varphi(\1)}$. Similairement, $\Last{\varphi(\1)}+\1=\First{\varphi(\2)}$. De plus, on sait que $\First{\varphi(\2)}=\Last{\varphi(\0)}+\2$, puisque $\varphi(\2) = \widehat{\varphi(\0)}$. En soustrayant $\1$ de chaque côté de ces deux dernières équations, nous obtenons $\Last{\varphi(\1)}=\Last{\varphi(\0)}+\1$, d'où $\Last{\varphi(\1)}=\First{\varphi(\1)}$. De la même manière, nous obtenons $\Last{\varphi(\0)}=\First{\varphi(\0)}$. À partir de ces équations, il s'ensuit que l'ensemble des premières
lettres est bien $\{\0, \1, \2, \3\}$.
\end{proof}

La proposition~\ref{morphismpara}, démontrée dans \cite{blondin2013combinatorial}, est l'idée principale sur laquelle nous nous appuyons pour la conception des algorithmes de factorisation de polyominos et l'étude de leur primalité. Ce fait indique en effet que les facteurs d'un polyomino parallélogramme quelconque commencent et se terminent par le même pas élémentaire.

Les morphismes parallélogrammes et homologues forment des sous-monoïdes du monoïde des morphismes sur $\Freeman$~:
\begin{prop}
Soit $\Mor$ l'ensemble des morphismes de $\Freeman$, $\Hom$ l'ensemble des morphismes homologues et $\Par$ l'ensemble des morphismes parallélogrammes. Alors $\Hom$ et $\Par$ sont des sous-monoïdes de $\Mor$ par rapport à la composition.
\end{prop}

\begin{proof}
Tout d'abord, l'élément neutre est le morphisme identité $\Id$ qui est à la fois un morphisme homologue et parallélogramme.

Soit $\varphi,\varphi' \in \Hom$. Alors pour chaque pas élémentaire $a \in \Freeman$:
$$\widehat{(\varphi \circ \varphi')(\bar{a})} = \widehat{\varphi(\varphi'(\bar{a}))} = \varphi(\widehat{\varphi'(\bar{a})}) = \varphi(\varphi'(a)) = (\varphi \circ \varphi')(a),$$
de sorte  que $\varphi \circ \varphi' \in \Hom$. Similairement si $\varphi,\varphi' \in \Par$, alors
$$\First{(\varphi \circ \varphi')(a)} = \First{(\varphi(\varphi'(a))} = \First{\varphi'(a)} = a.$$
Il est clair que $(\varphi \circ \varphi')(\0\1\2\3)$ est fermé. En outre, on montre que $(\varphi \circ \varphi')(\0\1\2\3)$ est auto-évitant, ce qui termine la démonstration.
\end{proof}

Chaque morphisme parallélogramme $\varphi$ est associé à un polyomino unique, dénoté par $\Poly(\varphi)$. Inversement, on pourrait être tenté de dire que chaque polyomino unique, dénoté par $\Poly(\varphi)$ est uniquement représenté par un morphisme parallélogramme. À cet effet, la condition (i) de la définition 2 assure que le mot de contour est traversé dans le sens horaire et que seulement un de ses conjugués est choisi. Cependant, même en tenant ces restrictions en compte, il existe des polyominos parallélogrammes avec deux factorisations parallélogrammes distinctes.
\begin{example}
Les morphismes parallélogrammes $\varphi$ définis par $\varphi(\0) = \0\1\0, \varphi(\1) = \1\2\1$
et  $\varphi'(\0) = \0\3\0, \varphi'(\1) = \1\0\1$,
produisent l'un et l'autre le pentamino $X$ (voir figure~\ref{pentamino}).
 
\begin{figure}[tbph]
\begin{center}

\includegraphics[scale=0.7]{Logos/pentamino}

\end{center}

\caption{Pentamino $\bf{X}$}
\label{pentamino}
\end{figure}
\end{example}


Les tuiles admettant deux factorisations distinctes non triviales sont appelées \textbf{doubles parallélogrammes} et ont été décrites dans \cite{blondin2013combinatorial}. De plus, il n'existe aucun polyomino parallélogramme décrit par plus de deux morphismes \cite{blondin2012parallelogram}.

Nous sommes maintenant prêts à définir les polyominos premiers et composés. Il devrait être noté que nous présentons une définition équivalente à celle donnée dans \cite{xavier} qui repose sur la notion de morphisme parallélogramme.

\begin{defn}
Soit $Q$ un polyomino quelconque et $\varphi$ un morphisme parallélogramme. Alors la \emph{composition de $\varphi$ et $Q$}, dénotée par $\varphi(Q)$, est le polyomino unique de mot de contour $\varphi(u)$, où $u$ est un mot de contour de $Q$.
\end{defn}

Des exemples sont présentés dans les figures \ref{morphismepara}, \ref{escargot}, \ref{aimant} et \ref{sapin}.

\begin{figure}[tbph]
\includegraphics[scale=0.9]{\string"Logos/Composition of a polyomino\string".png}

\caption[Construction d'un polyomino avec un polyomino parallélogramme]{Construction d'un polyomino avec un polyomino parallélogramme. Le
morphisme parallélogramme est illustré avec des points noirs. Le polyomino
composé résultant peut être pavé par des copies du polyomino parallélogramme.}
\label{morphismepara}
\end{figure}

\begin{defn} \label{D:premier}
 Un polyomino non unitaire $P$ est dit \emph{composé}
s'il existe un mot de contour $u$ et un  morphisme
parallélogramme $\varphi$ tel que:
\end{defn}
\begin{enumerate} 
\item  $\Poly(\varphi(u)) = P$; 
\item  $\Poly(u)$ n'est pas le carré unitaire; 
\item  $\varphi \neq \Id$.
\end{enumerate}
Sinon , $P$ est dit \emph{premier}.

En d'autres termes, un polyomino est premier si et seulement s'il ne peut pas être décomposé non trivialement comme le produit d'un morphisme parallélogramme et d'un autre polyomino. Il est utile de noter que le problème de décider si un polyomino est premier est au moins aussi difficile que le problème de décider si un nombre est premier.  En effet, on peut remarquer que le rectangle $1 \times n$ ayant le mot de contour $\0^n\1\2^n\3$ est premier si et seulement si $n$ est premier. Dans le même esprit, un mot de contour $u$ est appelé \emph{premier} si $\Poly(u)$ et un polyomino premier et un morphisme parallélogramme $\varphi$ est appelé \emph{premier} si $\Poly(\0\1\2\3)$ est un polyomino premier.

\section{Théorème de décomposition en facteurs premiers}


\subsection{Génération et factorisation de nombres premiers}


Dans la cryptographie traditionnelle, en vigueur avant
les années 1970, les opérations de chiffrement et de déchiffrement
étaient effectuées avec une même clé unique (voir figure~\ref{chiffsym}).  
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.35]{Logos/hmm-le-chiffrement-symetrique}
\end{center}
\caption[Chiffrement symétrique: La clé secrète qui chiffre est la clé secrète
qui déchiffre]{Chiffrement symétrique: La clé secrète qui chiffre est la clé secrète
qui déchiffre \url{<http://www.cyphercat.eu/expl_chiffrement_symetrique.php>}.}
\label{chiffsym}
\end{figure}
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.6]{\string"Logos/prob chiffrement syme\string".png}
\end{center}
\caption[Lacune du chiffrement symétrique]{Lacune du chiffrement symétrique \url{<http://www.kitpages.fr/fr/cms/93/chiffrements-symetriques-et-asymetriques>}}
\label{lacune}
\end{figure}
Cette façon restrictive d'opérer a incité deux chercheurs de l'université Stanford, Whitfield Diffie et Martin Hellman, à écrire en 1976 un célèbre article \cite{diffie1976new}, dans lequel ils suggèrent que peut-être le chiffrement et déchiffrement pourraient être faits avec une paire de clés différentes plutôt qu'avec la même clé. La clé de déchiffrement serait alors gardée secrète comme étant la paire de clé privée (voir figure~\ref{rsa}). La clé de chiffrement est publique sans compromettre la sécurité du déchiffrement.

La \emph{théorie des nombres} \index{théorie des nombres} s'avère utile en ce qui concerne la sécurité informatique. Le concept a été appelé la cryptographie à clé publique \index{cryptographie à clé publique} ou cryptographie asymétrique et repose sur la théorie des nombres\cite{rivest1978method}.  Elle permet de protéger les données sensibles telles que les numéros de carte de crédit.
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.7]{Logos/rsaalgo}
\end{center}
\caption[Cryptosystème à clé publique]
{ \emph{Les fonctions à sens uniques} sont des fonctions mathématiques telles qu'une fois appliquées à un message, il est extrêmement difficile de retrouver le message original. L'existence d'une \emph{brèche secrète} permet cependant à une personne de  décoder facilement le message grâce à un élément d'information. Les fonctions à sens unique avec brèche secrète sont à la base  de RSA. La fonction à sens unique à brèche secrète est la  \og clé publique \fg. L'élément d'information est la  \og clé privée \fg.  Ce cryptosystème à clé publique utilise effectivement deux clés: une publique et
une privée. Celles-ci sont générées à partir des nombres premiers.
Pour transmettre un message à Bob, Alice utilise la clé publique
de Bob. Seule la clé privée secrète de Bob peut déchiffrer le message.
 \url{<http://mbourgeois.developpez.com/articles/securite/pgp/>}.}
\label{rsa}
\end{figure}

La cryptographie asymétrique est donc basée sur la théorie des nombres premiers, et sa robustesse tient du fait qu’il n’existe aucun algorithme efficace de décomposition d’un nombre en facteurs premiers. Nous notons les observations suivantes :
\begin{obs}
La génération de nombres premiers est simple: Il est facile de trouver
un nombre premier aléatoire d'une grandeur donnée. Pour générer un
nombre premier aléatoire, on peut simplement générer des nombres aléatoires
de longueurs données et faire des tests de primalité sur ces nombres
jusqu'à ce qu'un nombre premier soit trouvé.
\end{obs}

\begin{obs}
 La multiplication est simple: compte tenu deux grands nombres premiers $p$ et $q$, il est facile
de calculer leur produit, $N = pq$ (complexité quadratique). \end{obs}
\begin{obs}
La factorisation est difficile: le problème consistant à retrouver $p$ et $q$ connaissant $N$ est difficile quand $p$ et $q$ sont grands. C’est le problème de  factorisation (voir Figure~\ref{primitive}). Pour preuve de la difficulté de ce problème, l'algorithme le plus efficace à ce jour, connu sous le nom de crible a une complexité super-polynomiale \cite{lenstra1993number}.
\end{obs}

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.6]{\string"Logos/La-factorisation\string".png}
\caption[primitive] {Une \emph{primitive}  est construite à l’aide de deux grands nombres premiers $p$ et $q$ distincts et de tailles
similaires. Le problème de la factorisation ci-dessus est la primitive RSA.}
\label{primitive}
\end{center}
\end{figure}



Il n'est pas toujours facile de déterminer si un nombre est 
premier. En fait, il semblerait que pour tester la primalité d'un
nombre, on aurait besoin de déterminer tous les facteurs du nombre
pour voir s'il existe un diviseur autre que le nombre en question
et 1. Des méthodes plus rapides pour les tests de primalité découvertes
dans les années $1970$ permettent de déterminer des nombres premiers
en exploitant certaines de leurs propriétés \cite{miller1976riemann}. Sans ces résultats
de détection de nombres premiers, beaucoup de clés publiques cryptographiques
actuelles ne seraient pas pratiques voire  obsolètes  du
fait que les méthodes efficaces de génération de grands nombres premiers
permettent de construire de longues paires de clés publiques/privées. 

Conséquemment aux longues paires de clés publiques/privées, les méthodes de calcul actuels pour déterminer si un grand nombre
est premier sont lentes et coûteuses. Par exemple, il a été récemment
estimé que la récupération des facteurs premiers d'un nombre de $1024$
bits prendrait une année sur une machine coûtant $10$ millions de dollars
US \cite{franke2005shark}. Un nombre de $2048$ bits nécessiterait plusieurs billiards de fois  plus de travail. Ces estimations actuelles sont ce à quoi on aurait
pu s'attendre dans les années $1970$ lorsque le problème a été proposé \cite{rivest1978method}.
Parallèlement, la taille recommandée des clés de chiffrement devient
de plus en plus importante en raison d'une puissance de calcul en croissance constante, soit de plus en plus d'outils puissants de cryptanalyse.
Ainsi, personne ne sait si on pourra découvrir
des longueurs de clés plus importantes dans les années à venir. 

\subsection{Théorème fondamental de l'arithmétique}


S'il existe un algorithme simple à mettre en place pour décomposer un nombre de taille raisonnable, cet algorithme se révèle rapidement inefficace, en termes de temps, pour de très grands nombres. La recherche d'algorithmes performants est donc très pertinente. En outre, si une méthode rapide était trouvée pour résoudre le problème de la factorisation des nombres entiers, alors plusieurs systèmes cryptologiques importants seraient cassés, incluant l'algorithme à clé publique RSA.

Après la définition \ref{D:premier}, on est naturellement amené à se demander si le théorème fondamental de l'arithmétique peut être étendu aux polyominos. Il y a deux conditions qui doivent être vérifiées~: l'\textbf{existence} d'une factorisation en nombres premiers et l'\textbf{unicité} de cette factorisation.

La première est facile à prouver:
\begin{thm} \label{T:existence}
Soit $P$ un polyomino non unitaire sans trou. Il y a deux cas possibles~:
\begin{enumerate}
  \item $P$ est un polyomino premier;
  \item Il existe des morphismes parallélogrammes premiers $\varphi_{\1}$, $\varphi_{\2}$, $\ldots$, $\varphi_{n}$ et un mot de contour premier $u$ tel que
  $$P = \Poly((\varphi_{\1} \circ \varphi_{\2} \circ \ldots \circ \varphi_{n})(u)).$$
\end{enumerate}
\end{thm}

\begin{proof}
Par induction sur le périmètre de $ P $. Si $ P $ est premier, alors
il n'y a rien à prouver. Sinon, il existe un morphisme parallélogramme
$\varphi \neq \Id$ et un mot de contour $u$, avec $\Poly(u)$ différent
du carré unité, tel que $P = \Poly(\varphi(u))$. Par induction appliquée
à $\Poly(u)$, il existe des morphismes parallélogrammes $\varphi_{\1}$, $\varphi_{\2}$, $\ldots$,
$\varphi_n$  et un mot de contour premier $v$ tel que
$$u = (\varphi_{\1} \circ \varphi_{\2} \circ \ldots \circ \varphi_{n})(v),$$
de sorte que $P = \Poly((\varphi \circ \varphi_{\1} \circ \varphi_{\2} \circ \ldots \circ \varphi_{n})(v))$. 
Si $\varphi$ est premier alors l'affirmation est prouvée. 
Sinon, en utilisant l'induction, on montre que $\varphi$ est égal à $\varphi'_{\1} \circ \varphi'_{\2} \circ \ldots \circ \varphi'_{k}$
pour des parallélogramme premiers $\varphi'_{\1}$, $\varphi'_{\2}$,$\ldots$, $\varphi'_{k}$.
 Ce qui conclut la preuve sur l'existence d'une factorisation première.
  \label{th7} 
\end{proof}

Le théorème \ref{T:existence} garantit l'existence d'une factorisation en polyominos premiers. Cependant, il semble plus difficile de démontrer si une telle factorisation est unique. D'un point de vue expérimental, nos explorations informatiques faites avec des polyominos de périmètre ne dépassant pas \textbf{900} n'ont pour le moment pas produit de contre-exemple. Nous sommes donc mené à proposer la conjecture qui suit:
\begin{conjecture}
Soit $P$ un polyomino composé quelconque. Alors, il existe un unique morphisme parallélogramme 
$\varphi_{\1}$ et un unique mot de contour $u$ premier tel que $P = \Poly((\varphi_{\1} \circ (u)))$
\end{conjecture}

\section{Algorithmes de factorisation}

Afin de déterminer si un polyomino $P$ est composé, il suffit de trouver un morphisme parallélogramme $\varphi\neq \Id$ et un mot de contour $u\not\in [\0\1\2\3]$  tel que $\varphi(u)$ soit le mot de contour de $P$. Si un tel morphisme est inexistant, alors nous pouvons en conclure que le polyomino est premier.

\subsection{Version naïve }

L'approche directe et simple consiste à essayer chaque factorisation possible jusqu'à ce qu'on trouve un morphisme parallélogramme capable de factoriser le mot de contour du polyomino. Cette approche brute de stockage d'occurrences, bien que coûteuse, présente un niveau raisonnable d'efficacité et nous garantit entre autres de couvrir tous les morphismes homologues existants appliqués au pas élémentaire si celui-ci est composé.

Pour réduire le nombre de cas à prendre en compte, on se limite à des facteurs commençant et se terminant par la même lettre. Plus précisément, soit $P$ un polyomino et $w$ l'un de ses mots de contour, pour tout $a \in \Freeman$, soit $F_a$ l'ensemble des facteurs de $w^2$ commençant et se terminant par $a$. Les étapes ci-dessous peuvent être utilisées pour factoriser $P$~:
  
\begin{enumerate}
  \item Calculer  les occurrences $u \in F_{\0}$, $v \in F_{\1}$;
  \item Soit  $u \in F_{\0}$, $v \in F_{\1}$;
    \item Soit $\varphi$ le morphisme parallélogramme induit par  $u$ et $v$;
    \item S'il y a un conjugué quelconque $w'$ de $w$ tel que $w = \varphi(w')$, alors retourner $(\varphi,u)$.
    \item Autrement, répéter les étapes 2--4 jusqu'à ce que tous les $u$ et $v$ possibles soient épuisés.
\end{enumerate}
  

\begin{comment}
\item Fonction \textbf{occurrences} (voir Algorithme~\ref{alg:occurencespretraitement})~:
\item Stocker les occurences dans $\Theta_y$ (Ligne 8);
  \item Retourner $\Theta_i$ (Ligne 10).
  \item Fonction \textbf{prétraitement} (voir Algorithme~\ref{alg:occurencespretraitement})~:
  \item Fonction \textbf{prétraitement} (voir Algorithme~\ref{alg:occurencespretraitement})~:

 \item Vérifier si les ocurrences collectées dans $\Theta_y$ construisent un morphisme parallélogramme $\varphi$ induit par $u \in \Freeman_{\0}$, $v \in \Freeman_{\1}$, $w \in \Freeman_{\2}$, $x \in \Freeman_{\3}$ (lignes 12--18);
  \item Retourner $\varphi$ (ligne 19).


\item Fonction \textbf{découpage} (voir Algorithme~\ref{alg:decoupagedecodage})~:

  \item Pour tout conjugué $w'$ de $w$, vérifier si $w'$ est décodable en fonction de $\varphi$ (lignes 33--48);
  \item Retourner $(\varphi,u)$ (ligne 44).
\item Sinon, répéter les étapes 1-2-3 jusqu'à ce que tous les $u$ et $v$ possibles soient épuisés.
\end{comment}


La complexité de l'algorithme ci-dessus est clairement polynomiale.
\begin{thm}
Tout polyomino $P$ peut-être factorisé en produit de polyominos premiers en $\mathcal{O}(n^7)$ où $n$ est le périmètre de $P$.
\end{thm}

\begin{proof}
Soit $w$ tout mot de contour de $P$. L'étape $1$ est realisée en
$\mathcal{O}(n^2)$ puisqu’il y a $\mathcal{O}(n^2)$  facteurs commençant
et se terminant par le même pas élémentaire dans tout mot de \foreignlanguage{english}{$\Freeman$}. Donc, il y a au plus $n^2$ valeurs possibles.
Les étapes $2-4$ sont alors répetées en $\mathcal{O}(n^4)$ puisqu'il
y a au plus $n^2$  comparaisons
possibles considérant les chemins horizontaux et verticaux. La construction
de $\varphi$ à l'étape 3 est alors faite en temps constant alors
que l'étape 4 prend $\mathcal{O}(n)$ puisque celle-ci doit être effectuée
pour chaque conjugué de $w$. En conséquence, décomposer $P$ par
$\varphi(u)$, pour un quelconque morphisme parallélogramme $\varphi$
et un mot de contour $u$ est fait en $\mathcal{O}(n^5)$. Finalement,
quand la liste des $\varphi(u)$ est formée pour chaque $a \in \Freeman$,
il reste à vérifier si $w$ est décodé par $\varphi$, en fissurant
$w$, considérant ainsi la longueur du morphisme homologue pour les
chemins horizontaux et verticaux. Cette opération est faite au plus
en temps linéaire. Dès lors que cela doit être répété aussi longtemps
que $\varphi$ ou $u$ n'est pas premier, la complexité globale est
$\mathcal{O}(n^7)$. 

Par conséquent:\end{proof}
\begin{cor}
Étant un polyomino $P$ de périmètre $n$, il peut être décidé en
temps polynomial par rapport à $n$ si $P$ est
premier ou composé.
\end{cor}

\IncMargin{1em}
\begin{algorithm}[Hhtbp]  
\footnotesize
\SetKwFunction{occurrences}{OCCURENCES}
\emph{\bf{Donn\'{e}es:} $w$ $est$ $ferm\acute{e}$, $ mot$ $de$ $freeman$, $simple$ $et$ $auto-\acute{e}vitant$ }\; 
\bf{Fonction} \occurrences{$w$ : mot de contour, $a \in \Freeman$}{} \\
\Deb{
\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up} 
\SetKwInOut{Input}{input}\SetKwInOut{Output}{output} 
\Sortie{$F_{a}$} 
$x=\0$\\
\Pour{$i \leftarrow 1$ \bf{\`{a}} $|w|$}{  
\Pour{$j \leftarrow i$ \bf{\`{a}} $|w|$}{
\Si(){($w[i]=a$ \bf{et} $w[j]=a$ \bf{et}  $i\leq j $)}
{

$occurence \leftarrow w[i:j]$\\
$occurence \in F_{a}$\\
$x++$
}


}
}

\Retour{$F_{a}$}
}
\BlankLine
\SetKwFunction{principale}{principale}
\SetKwFunction{spliting}{FACTORISATION}
\bf{Fonction} \principale{$w$ : mot de contour, $a \in \Freeman$}{}\\
\Deb
{
\SetKwData{Left}{left}\SetKwData{This}{this}\SetKwData{Up}{up} 
\SetKwInOut{Input}{input}\SetKwInOut{Output}{output} 
\Sortie{$\varphi$} 
$F_{\0}=$ \occurrences($w, \0$)\\
 $F_{\1}=$ \occurrences($w, \1$)\\
 
\Pour{$i \leftarrow 1$ \bf{\`{a}} $|F_{\0}|$} {

\Pour{$j \leftarrow 1$ \bf{\`{a}} $|F_{\1|}$} {

$u = F_{\0}(i)$\\
$v = F_{\1}(j)$\\
$\varphi(\0)=u$\\
$\varphi(\1)=v$\\
$factorisation$=faux\\
\bf{FACTORISATION}($w,\varphi(\0),\varphi(\1)$)\\
\Si(){($factorisation$==vrai)}
{
\Retour{$\varphi$} \tcc{Le morphisme parallélogramme est complètement défini et non trivial, on le retourne. Donc $w'=\varphi(w)$ est un mot de contour fermé et auto-évitant. On a alors une figure discrète composée}
}
\Sinon()
{
\Retour{$\emptyset$} \tcc{Si $\varphi$ est un ensemble vide alors on peut conclure qu'on a alors une  figure discrète première }
}

 
} 
}

}
\BlankLine

\caption{Algorithme naïf (occurrences, principale) }\label{alg:occurencespretraitement}
\end{algorithm}




\DecMargin{1em}

\IncMargin{1em}
\begin{algorithm}[Hhtbp]   
\footnotesize

\bf{Fonction} \spliting{$w$ : mot de contour, $\varphi(\0)$, $\varphi(\1)$}{}\tcc{On considère ici les caractéristiques de la longueur de $\varphi(\0)$ et $\varphi(\1)$ pour  factoriser $w$.  }
\Deb{
\SetKwInOut{Input}{input}\SetKwInOut{Output}{output} 
\Entree{mot de contour, dimension  x, dimension y} \tcc{$x=|\varphi(\0)|$}
\tcc{$y=|\varphi(\1)|$} 
\Pour{$i \leftarrow 1$ \bf{\`{a}} $|w|$} {
y=\0\\
\Si(){$w_i==\0$ \bf{ou} $w_i==\2$}
{
$scission[y]=w[i...x]$, $i=x$, $x=+i$
}
\Si(){$w_i==1$ \bf{ou} $w_i==3$}
{
$scission[y]=w[i...y]$, $i=y$, $y=+i$
}
}
\Pour{$i \leftarrow 1$ \bf{\`{a}} $|scission|$} 
{
$w'\leftarrow$ $scission[i]$[$premier$ $indice$] \tcc{On récupère le premier indice de chaque découpage et on s'assure que $w'$ est fermé et auto-évitant. Si oui $w = \varphi(w')$.}
}

\Si(){w' $est$ $ferm\acute{e}$, $simple$ \bf{et} $auto-\acute{e}vitant$ == $vrai$ }
{
polyomino est composé\\
\Retour $w'$\\
$factorisation \leftarrow $\bf{vrai}
}





\Si(){$factorisation$==faux}{polyomino est premier\\}

}
\BlankLine

\caption{Algorithme naïf (Factorisation) }\label{alg:decoupagedecodage} 

\end{algorithm}
\DecMargin{1em}

\IncMargin{1em}

\DecMargin{1em}


\selectlanguage{french}%
\newpage


L'algorithme a été implémenté en Java et testé sur des polyominos premiers
et composés. Les détails de l'évaluation empirique sont
donnés dans le chapitre 4.
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=1.1]{Logos/escargot}
\end{center}
\caption[Illustration du travail de notre algorithme de factorisation
de polyominos: figure discrète composée en forme d'escargot]{{\small{Illustration du travail de notre algorithme de factorisation
de polyominos (implémentation en Java en utilisant le module Turtle
de Python). Soit la factorisation
d'une figure discrète composée en forme d'escargot (Figure $a$)
de mot de contour $\3\3\2\3\2\2...\0\0$ et de longueur du mot de
contour $L=268$, dans le sens antihoraire. L'algorithme détecte la
figure $a$ comme étant l'application du morphisme $\varphi$ (Figure
$d$), representé par le morphisme parallélogramme $Q$ (Figure $c$),
appliqué au contour de la figure discrète première $P$ en forme d'escargot
(Figure $b$). Le tout en quelques millisecondes. }}}
\label{escargot}
\end{figure}

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=1.4]{Logos/aimant}
\end{center}
\caption[Illustration du travail de notre algorithme de factorisation
de polyominos: figure discrète composée en forme d'aimant]{{\small{Illustration du travail de notre algorithme de factorisation
de polyominos (implémentation en Java en utilisant le module Turtle
de Python). Soit la factorisation
d'une figure discrète composée en forme d'aimant (Figure $a$) de
mot de contour $\3\0\3\0\1\0\1...\3\2$ et de longueur du mot de contour
$L=48$, dans le sens antihoraire. L'algorithme détecte la figure
$a$ comme étant l'application du morphisme $\varphi$ (Figure $d$),
representé par le morphisme parallélogramme $Q$ (Figure $c$), appliqué
au contour de la figure discrète première $P$ en forme de vaisseau
spatial (Figure b). Le tout en quelques millisecondes. }}}
\label{aimant}
\end{figure}

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=1.1]{Logos/sapin}
\end{center}
\caption[Illustration du travail de notre algorithme de factorisation
de polyominos: figure discrète composée en forme de sapin]{{\small{Illustration du travail de notre algorithme de factorisation
de polyominos (implémentation en Java en utilisant le module Turtle
de Python). Soit la factorisation
d'une figure discrète composée en forme de sapin (Figure $b$) de
mot de contour $\0\0\0\0\1\1...\3\2\3$ et de longueur du mot de contour
$L=120$, dans le sens antihoraire. L'algorithme détecte la figure
$c$ comme étant l'application du morphisme $\varphi$ (Figure $d$),
representé par le morphisme parallélogramme $Q$ (Figure $c$), appliqué
au contour de la figure discrète première $P$ en forme de sapin (Figure $a$). Le tout en quelques millisecondes. }}}
\label{sapin}
\end{figure}

\subsection{Amélioration de l’algorithme naïf}

Une complexité de $\BigO(n^7)$ est plutôt grande et il est raisonnable d'espérer la réduire. Plus spécifiquement, au lieu d'énumérer tous les facteurs possibles $u$ dans $\Freeman_{\0}$ et $v$ dans $\Freeman_{\1}$, le choix devrait plutôt s'opérer selon que $u$ et $v$ surviennent de façon contiguë dans le mot en traitement. Dès lors, cela donne l'approche améliorée suivante.

Comme première étape, nous choisissons n'importe quel mot de contour $w$ commençant par $\0$. La première idée consiste à essayer de diviser $w$ par des blocs commençant et se terminant par le même pas élémentaire, conformément à la proposition $2$. Il suffit donc de regarder toutes les occurrences de $\0$ pour déterminer un $\varphi(\0)$. Quand un tel bloc est choisi, alors $\varphi(\0)$ et $\varphi(\2)$ sont complètement déterminés.

L'étape suivante consiste à vérifier le pas élémentaire suivant le premier bloc. Si celui-ci est $\0$ ou $\2$ alors nous vérifions si les lettres suivantes ou le bloc suivant correspondent à $\varphi(\0)$. Si tel n'est pas le cas, alors nous devons choisir une autre valeur $\varphi(\0)$. D'autre part, s'il y a une correspondance, alors on passe au bloc suivant. Nous répétons les étapes précédentes jusqu'à ce que nous atteignions la lettre $\1$ ou $\3$. De la même manière que pour la lettre $\0$, nous essayons ensuite chaque bloc possible pour chaque $\1$ ou $\3 $. Quand un tel bloc est choisi, alors $\varphi(\1)$ et $\varphi(\3)$ sont complètement déterminés.  Quand les quatre images de $\varphi$ sont choisies, il ne reste plus qu'à vérifier si le mot de contour peut être factorisé comme un produit des quatre blocs (cette étape est appelée étape de décodage).

Cela nous mène naturellement à l'algorithme \ref{alg:algo_improved} qui permet de factoriser tout polyomino composé en produit de deux polyominos plus petits.
\selectlanguage{english}%
\IncMargin{1em}
\begin{algorithm}
\scriptsize
\SetKwFunction{FACTORIZE}{FACTORIZE}
\SetKwFunction{OCCURRENCES}{OCCURRENCES}
\SetKwFunction{CONJUGATE}{CONJUGATE}
\SetKwFunction{FACTORIZERECT}{FACTORIZERECT}
\emph{\bf{Donn\'{e}es:} $w$ $is$ $closed$ $,$ $freeman$ $word$, $simple$ $and$ $self$ $avoidant$ }\;
\bf{Function} \FACTORIZE{$w$ :mot de contour}{}\\
\Deb{
\Pour{$i \in $ \OCCURRENCES(\bf{\0},$w$)}{\tcc{\OCCURRENCES permet de construire les blocs et prend comme argument $l\in \Freeman$. Initialement, comme argument $\bf{\0}$, la fonction construit chaque bloc commençant et se terminant par  $\bf{\0}$.  }
$u \leftarrow$ \CONJUGATE($w,i$)\tcc{ \CONJUGATE construit les conjugués $\varphi_{0}$ et $\varphi_{2}$ pour les chemins horizontaux, $\varphi_{1}$ et $\varphi_{3}$ pour les chemins verticaux.}
$\varphi \leftarrow$ \FACTORIZERECT($\emptyset,\0$)\tcc{\FACTORIZERECT est imbriquée dans \FACTORIZE. La fonction essaie, pour $\varphi$ non complètement défini et $\varphi(\0)$ déterminé, son conjugué $\varphi(\2)$ aussi déterminé, chaque bloc possible pour chaque $\1$ ou $\3$. Cela permet de décoder le mot de contour pour voir $\varphi(w)=w'$.}
\Si(){$\emptyset \neq \varphi$}
{
\tcc{Si le morphisme parallélogramme est complètement défini et non trivial, on le retourne. Donc $\varphi(w)=w'$ qui est un mot de contour fermé et auto-évitant. On a alors une figure discrète composée. }
\Retour{$\varphi$}
} 
}
\Retour{$\emptyset$} \tcc{Si $\varphi$ est un ensemble vide alors on peut conclure qu'on a alors une  figure discrète première.}
}
\BlankLine
\BlankLine
\bf{Fonction} \FACTORIZERECT{$w$ :mot de contour $\varphi$ : morphism, $i$ : integer)}{}\\
\Deb{
\Si(){$i\geq |w|$}{
\tcc{Nous vérifions tout d'abord si le décodage est complet.}
\Si(){$\varphi$ est complètement défini et non trivial.}
{
\Retour{$\varphi$} \tcc{Oui, le décodage est complet et il existe un $\varphi$.}
}
\Sinon{
\Retour{$\emptyset$}\tcc{Non, le décodage est incomplet. $\varphi$ est un ensemble vide.}
}
}

\Sinon{
$l \leftarrow$ $w$[$i$]\\
\Si(){$\varphi(l)$ est défini}
{
\tcc{On vérifie si tous les prochains blocs correspondent à $\varphi(l)$. }
$k \leftarrow | \varphi[(l)]|$\tcc{On pose quand la longueur du morphisme homologue  appliquée sur le chemin vertical ou horizontal. }
\Si(){$k>|w|-i$ \bf{ou} $w[i:i+k]$ }
{
\tcc{Le décodage est incomplet ou mauvais si $k>|w|-i$ ou k>$w[i:i+k]$. }
\Retour{$\emptyset$}
}
\Sinon{
\tcc{Autrement, le décodage est bon et on retourne \FACTORIZERECT avec comme argument $\varphi$ et la position $i + k$. }
\Retour \FACTORIZERECT($\varphi, i + k$)
}
}
\Sinon{
\tcc{Nous considérons toutes les constructions possibles du prochain bloc.}
\Pour{$j \in $ \OCCURRENCES($l ,w[i :|w|-1 ]$)}{ 
$\varphi(l) \leftarrow w[i:j]$\\
$\varphi(\bar{l}) \leftarrow \widehat{w[i:j]}$\\
$\varphi \leftarrow $\FACTORIZERECT($\varphi,j + 1$)\\
\Si(){$\varphi$ $is$ $not$ $trivial$}{
\Retour $\varphi$
}
\Sinon{\Retour{$\emptyset$}}
}
}
\Retour{$\emptyset$}
}
}
\caption{ Amélioration de l’algorithme naïf}\label{algo_polyominos} 
\label{alg:algo_improved}

\end{algorithm}
\DecMargin{1em}
\selectlanguage{french}%

\begin{thm}
Tout polyomino $P$ peut-être factorisé en produit de polyominos premiers en $\mathcal{O}(n^5)$ avec $n$ étant le périmètre de $P$.
\end{thm}

\begin{proof}
Soit $w$ le mot de contour de $P$ commençant par $\0$. En choisissant chaque occurrence de $\0$ en $w$ pour construire $\varphi(\0)$, il y a au plus $n$ valeurs possibles (et puis $\varphi(\2)$ est déterminé). Une fois $\varphi(\0)$ est choisi, il y a au plus $n$ valeurs possibles pour $\varphi(\1)$ (et puis $\varphi(\3)$ est déterminé). Enfin, quand $\varphi(a)$ est connu pour chaque $ a\in\Freeman$, il reste à vérifier si $w$ pourrait être décodé de $\varphi$, ce qui se fait en temps linéaire tout au plus. Par conséquent, il peut être décidé dans $\mathcal{O}(n^3)$ si $w$ est décodable selon certains morphismes parallélogrammes $\varphi $. Le test doit être effectué pour chaque conjugué de $w$ commençant par $\0$, on obtient alors $\mathcal{O}(n^4)$. Enfin, en répétant cette réduction jusqu'à ce que les objets ne soient plus décomposables, on obtient la complexité affirmée $\BigO(n^5)$.
\end{proof}

\section{Discussion}

La limite $\BigO(n^5)$ semble assez élevée et il n'est pas évident qu'elle puisse être atteinte en pratique. Pour appuyer cette impression, étudions l'algorithme amelioré pour un polyomino $P$ ayant un mot de contour 
$$w = \0^k\1^k\2^{k-1}\3\2\3^{ k-1}.$$
Soit $n=|w|=4k$. Comme $ P $ est un carré $(k \times k) -1$ cellules, on pourrait prouver qu'il est premier. Pour faire en sorte que l'algorithme prenne autant de temps que possible, supposons que $k$ ait $d$ diviseurs.
Ensuite l'algorithme va essayer le $k$ conjugué  de $w$ commençant 
par $\0$ et construira $d$ images pour $\varphi(\0)$. De même,
nous devrons considérer $d$ images possibles pour $\varphi(\1)$.
Le décodage étant effectué en temps linéaire, nous obtenons une limite
globale $\BigO(kd^2n)=\BigO(n^2d^2)$. Cependant $d$ est en général
beaucoup plus petit que $k$. Dès lors, il est facile de voir par
exemple que $d\leq\sqrt{k}$ (limites strictes de la théorie des
nombres). Par conséquent, on obtient dans ce cas une limite de $\BigO(n^3)$.

Nous pensons qu'une amélioration significative pourrait réduire la
limite théorique à $\BigO(n^4)$ ou même $\BigO(n^3)$  en prenant
en compte les répétitions de certains facteurs. Par exemple, quand
nous essayons de factoriser le polyomino $P$  du chapitre $4$ par $\varphi(\0)=\0$
et que nous lisons le facteur $\0^k$, il serait plus efficace de
garder en mémoire le fait que $\varphi(\0)$ ne peut etre construit
que par des puissances de $\0$ qui divisent $k$. 





