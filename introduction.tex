\chapter{Introduction}

Ce mémoire s'inscrit dans le domaine de la géométrie discrète, c'est-à-dire la modélisation d'objets continus à l'aide d'une représentation discrète. Cette branche de l'informatique a connu un essor remarquable avec l'avènement des ordinateurs~: étant donné la grande quantité de données numériques de nature géométrique, il est fondamental de développer des algorithmes traitant efficacement ces données et les problèmes qu'elles soulèvent. À titre d'exemple, la géométrie discrète est utilisée en synthèse d’images \cite{glassner1995principles}, dans le contrôle à distance, en télédétection par satellite \cite{dagorne1989modelisation}, en morphologie \cite{lu2003face}, en météorologie \cite{richardson2007weather} et en géomorphologie \cite{dikau1989application}. 

En géométrie discrète, on propose habituellement des représentations discrètes qui s'appliquent à un traitement numérique tout en conservant les propriétés essentielles de leurs antécédents continus. Dans cette perspective, Freeman a proposé en 1961 une façon pratique d'encoder tout chemin dans le plan discret comme une suite des quatre déplacements élémentaires $\{\rightarrow, \uparrow, \leftarrow, \downarrow\}$ par l'alphabet $\{\0,\1,\2,\3\}$. Ce chemin peut alors être représenté par un mot, ce qui permet d'étudier l'objet géométrique qu'il encode d'un point de vue combinatoire. Une \emph{figure discrète} \index{figure discrète} peut donc être vue comme un ensemble de mots munis chacun d'un point de départ, où le mot en question décrit le contour d'une région fermée.

Il convient de mentionner que, suite au développement de ces nouveaux champs de recherche sur le traitement d’images numérisées, la communauté scientifique s’est organisée en associations, parmi lesquelles on compte l’association scientifique à but non lucratif, créée en janvier $1978$, appelée \textbf{International Association for Pattern Recognition} dont le mandat est de promouvoir la reconnaissance par motifs \cite{iarp}. La reconnaissance par motifs est utilisée dans plusieurs contextes, tels que la robotique. Il s'agit donc d'un domaine qui regroupe une importante communauté de chercheurs, dont le sujet a été l'objet de plusieurs conférences. Par exemple, la conférence internationale DGCI (\textbf{Discrete Geometry for Computer Imagery}) \cite{dgci}  est centrée sur les aspects théoriques de la reconnaissance de motifs.  PRIB (\textbf{Pattern Recognition in Bioinformatics}) \cite{prib} traite de la présentation, de la discussion et de l'application de méthodes de reconnaissance par motifs en bio-informatique.

Le nom \emph{polyomino} a été proposé par Golomb en 1953 \cite{golomb},  puis ultérieurement popularisé par Gardner, très actif dans les mathématiques récréatives durant une bonne partie du 20e siècle. Les polyominos sont très connus du grand public. En effet, il suffit de penser au jeu Tétris qui consiste à remplir une matrice par des tétraminos (voir figure~\ref{tetris}). 
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.3]{Logos/tetris}
\end{center}
\caption[Le jeu vidéo Tetris]{Le jeu vidéo Tetris, qui a connu une popularité immédiate dans le monde entier à la fin des années 80  \url{<http://www.le-serpent-retrogamer.org/un-an-une-tof-1984-tetris/>}}
\label{tetris}
\end{figure}

Il existe en outre des jeux de société très populaires utilisant le polyomino comme unité de base, tels que \og Caminos \fg{}, qui est un jeu d’orientation dans l’espace consistant à tracer un passage, d’un bord à l’autre du plateau de jeu, avec des blocs de même couleur (voir figure~\ref{caminos}). De la même façon, le jeu Kaléidoscope, un jeu de structuration spatiale (voir figure~\ref{kaleidoscope}) consiste à reproduire un motif (un rectangle par exemple) à partir de $18$ pièces, chacune unique.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.3]{Logos/caminos}
\caption[Le jeu \og Caminos \fg{}]{Le jeu \og Caminos \fg{}. La règle est simple, être le premier à relier de manière ininterrompue deux côtés opposés du plateau avec des pièces de la couleur choisie  \cite{caminos}. }
\label{caminos}
\end{center}
\end{figure}

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.3]{Logos/kaleidoscope_1}
\caption[Un kaléidoscope]{Un kaléidoscope (kalos signifie « beau », eidos « image », et skopein
« regarder »).  À la base c'est un casse-tête pour jouer en solitaire. Le but du jeu est de reconstituer le plateau selon un plan déterminé avec des pièces qui sont des polyominos \cite{kaleidoscope}. }
\label{kaleidoscope}
\end{center}
\end{figure}

Le célèbre algorithme \og Dancing links \fg{} (liens dansants) proposé par D.  Knuth en 2000 a été initialement conçu dans l'objectif de résoudre un problème de pavage d'une forme à l'aide de polyominos ou de \emph{polycubes} (la version tridimensionnelle des polyominos). Il consiste à résoudre l'assemblage d'un puzzle par pavage efficace de polyominos \cite{knuth} (l’implémentation d’un tel solveur peut être trouvée dans \og Tiling solver in Sage, 2011 \fg{} \cite{solver}). 

\section{Revue de littérature}

Plusieurs chercheurs se sont intéressés à la modélisation des figures discrètes. Comme il s'agit d'un domaine vaste, nous nous concentrons sur les plus pertinents d'entres elles pour ce mémoire.

Un des exemples de la littérature qui établit un lien entre combinatoire des mots et géométrie discrète est dû à Vuillon et Berthé qui ont proposé une correspondance entre les plans discrets et des mots bidimensionnels sur un alphabet à trois lettres \cite{berthe2000tilings}. En dimension un, ce sont sans aucun doute les \emph{mots sturmiens} qui en sont les meilleurs exemplaires, puisqu'ils décrivent une \emph{discrétisation} d'une droite. Une propriété importante de ces mots est qu'ils présentent de nombreux facteurs palindromiques, c'est-à-dire des mots qui se lisent de la même façon dans les deux directions (comme \emph{radar}). Or, les \emph{palindromes} \index{palindromes} sont des indicateurs précieux de la structure de plusieurs mots et permettent des interprétations notamment en géométrie discrète. En effet, les mots sturmiens, étant riches en palindromes, induisent des symétries qui se traduisent sur les objets géométriques qu'ils représentent.

En $2005$, Brass et al. considèrent un grand choix de problèmes rencontrés par les chercheurs dans le domaine de la géométrie discrète et leurs résolutions par l’utilisation du modèle combinatoire \cite{brass2005research}. Leur ouvrage \og Research problems in discrete geometry \fg{} est le résultat d'un projet de 25 ans, initié par Leo Moser. On y décrit les frontières et l'avenir de la recherche sur la géométrie discrète. Il s'agit d'une collection de plus de 500 problèmes ouverts dans ce domaine. Les différents chapitres offrent un large aperçu sur le domaine, avec des détails historiques et les solutions partielles les plus reconnues apportées à ces problèmes. Certains problèmes sont notoirement difficiles et intimement liés à des questions profondes dans d'autres domaines des mathématiques.

Le \emph{mot de contour} \index{mot de contour} peut être vu comme un codage des polyominos par un alphabet à quatre lettres décrivant les quatre déplacements élémentaires : droite, haut, gauche et bas. Les lettres $\0,\1,\2,\3$ encodent respectivement ces déplacements et $\{\0,\1,\2,\3\}$ est appelé \emph{alphabet de Freeman}. L'avantage principal de cette représentation est qu'il est alors possible de considérer l'alphabet comme le groupe $\Z_4$. De plus, on peut extraire plusieurs informations sur le polyomino simplement en considérant son mot de contour. À titre d'exemple, Brlek et al. ont proposé en 2009 un algorithme linéaire (en temps et en espace) pour détecter les points d'intersections de chemins dans un espace discret dimension de $\Z^d$ \cite{brlek2009linear}. La puissance de l'algorithme repose principalement sur l'utilisation d'un arbre radix. En outre, cette structure de données peut sans aucun doute s'adapter à d'autres algorithmes, tels que l'intersection et la réunion de figures discrètes \cite{morrison1968patricia}.

Les polyominos sont aussi des objets de base dans l'étude des pavages. Des pavages de formes très variées sont utilisés depuis l’Antiquité pour l’aménagement et la décoration (carrelages, frises, peintures). Aussi, on rencontre dans la nature des exemples remarquables de pavages réguliers et irréguliers, comme dans les nids d’abeille ou dans les réseaux cristallins. La recherche permet de comprendre ces matériaux afin de les utiliser dans la fabrication de matières synthétiques, d'alliages ou de matériaux ultra-légers. 

Intuitivement, une \emph{tuile} \index{tuile} est une région plane dont la frontière est un polygone fermé. Nous savons, depuis 1970, que le problème du pavage d’un plan par des polyominos libres (polyominos qui peuvent subir une rotation ou une réflexion) choisis à partir d'un ensemble fini est indécidable \cite{undecidable}. Et même quand on se restreint à une seule tuile, la décidabilité du problème du pavage est inconnue. De même, le problème de savoir si un polyomino donné pave un autre polyomino est NP-complet \cite{moore2001hard}. Dans le cas où aucune réflexion ou rotation d'un polyomino libre n’est permise, le problème devient considérablement plus simple. Non seulement cela est décidable, mais on peut le déterminer en temps polynomial: une borne $O(n^2)$ est démontrée dans \cite{gambini2007algorithm}, réduite à $O(n)$ dans \cite{PSC2006h}. 

En effet, l'idée de base vient d'un article de Beauquier et Nivat qui ont caractérisé ces objets par la forme de leur contour. Ils démontrent entre autres que ces polyominos peuvent nécessairement être divisés en quatre ou six morceaux parallèles par paires \cite{bn}. En 2006, Brlek et Provençal considèrent  le problème de déterminer si un mot de contour d'une figure discrète donné pave le plan par translation \cite{PSC2006h}. 
 
D'un point de vue algorithmique, soit $n$ la longueur du mot de contour d'un  polyomino $p$, la caractérisation Beauquier-Nivat fournit un algorithme naïf déterminant si $p$ est pas défini en $O(n^4)$. Gambini et Vuillon  utilisent cette caractérisation et proposent en 2007 un algorithme pour décider si un polyomino pave le plan donné par translations en $O(n^2)$ \cite{gambini2007algorithm}.  Par la suite, en 2009, pour les polyominos parallélogrammes, Brlek et al. proposent un algorithme de détection en temps linéaire \cite{brlek2009tiling}, améliorant l'algorithme quadratique de Gambini et Vuillon. Leur approche est inspirée par l'algorithme linéaire de Gusfield et Stoye  pour détecter les répétitions en tandem dans un mot \cite{gusfield2004linear} et par l'algorithme linéaire utilisé pour détecter des répétitions avec gaps tel que montré dans  Lothaire \cite{lothaire1983combinatorics}. Plus précisément, par une utilisation intelligente de l'arbre des suffixes  \cite{gusfield1997algorithms}, il performe en temps constant  un pré-traitement linéaire sur les mots des polyominos parallélogrammes $u$ et $v$. Dans le cas des polyominos hexagonaux, la complexité est linéaire si le nombre de répétitions de sous-motifs est borné par une constante \cite{brlek2009tiling}.

Dans \cite{blondin2013combinatorial}, les auteurs examinent le problème de l'énumération des polyominos appelés \emph{doubles parallélogrammes} \index{doubles parallélogrammes} (voir figure~\ref{doublepara}), qui sont des polyominos engendrant deux pavages parallélogrammes distincts. Afin de prouver l'un des principaux résultats de l'article, les auteurs se basent sur le concept de polyominos premiers et composés, introduit par Provençal dans sa thèse de doctorat \cite{xavier}.
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.8]{\string"Logos/double parallele\string".png}
\caption[Deux doubles parallélogrammes et le pavage qu’ils induisent]{Deux doubles parallélogrammes et le pavage qu’ils induisent. Les points noirs indiquent les points partagés par quatre copies de la tuile. (a) Une tuile de Christoffel. (b) Une tuile de Fibonacci (voir \cite{bbgl09} pour plus de détails).}
\label{doublepara}
\end{center}
\end{figure}

\section{Problématique}

On dit d'un polyomino qu'il est \emph{premier} \index{premier} s'il est impossible de le paver à  l'aide d'un autre polyomino différent du carré unité. Autrement, il est dit \emph{composé}\index{composé}. De façon équivalente, cela veut dire qu'il ne peut être obtenu à partir d’une autre figure plus petite en remplaçant les pas élémentaires par des chemins plus complexes.

En combinatoire des mots, cela signifie qu’on ne peut obtenir le mot de contour à partir d’un autre en lui appliquant un \emph{morphisme homologue}\index{morphisme homologue}, soit une substitution telle que les chemins codant la figure discrète sont les mêmes à ordre de parcours inversé. 

Afin d'étudier les propriétés d'un polyomino premier, il faut d'abord concevoir un algorithme permettant de décider s'il l'est. Il s'agit d'une des contributions principale de ce mémoire. En outre, nous proposons un algorithme qui décompose n'importe quel polyomino en polyominos premiers. Nous proposons également une généralisation du théorème fondamental de l'arithmétique pour les polyominos.

Le premier chapitre est consacré à la définition des objets combinatoires et géométriques qui seront étudiés et employés par la suite. Les définitions et la notation relatives à la combinatoire des mots sont en majeure partie tirées des livres de M. Lothaire.

Le chapitre 2 est consacré à l'étude d'une classe de morphismes particuliers appelés \emph{morphismes homologues}. Celle-ci permet alors de définir la notion de polyominos premiers et composés. On y étudie aussi les aspects algorithmiques soulevés par ces problèmes.

Le chapitre 3 est consacré à l'évaluation empirique des concepts théoriques de nos deux algorithmes de factorisation proposés dans le chapitre 2.

Les résultats de ce mémoire ont été présentés en mars 2014 à la conférence \textbf{Language and Automata Theory and Applications (LATA)}, une conférence annuelle internationale organisée par le Groupe de recherche sur la linguistique mathématique (GRLMC) \cite{masse2014arithmetics} et dont les actes sont publiés par Springer.



