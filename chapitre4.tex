
\chapter{Discussion sur l'intégration de fondements théoriques}
 La plupart des algorithmes de chiffrement asymétrique à l'instar de RSA reposent sur des notions d'arithmétiques élémentaires et modulaires (notion de congruence\index{congruence}, les nombres premiers, division euclidienne, pgcd, équivalence, transitivité, etc).  Dans ce chapitre, nous discutons de l'applicabilité de ces outils dans l'arithmétique des polyominos.  On dit que deux nombres entiers $a$ et $b$ sont congrus modulo $n$ si $b - a$ est divisible par $n$. 
\section{RSA }
 Dans cette section, nous parlons des différents théorèmes arithmétiques et de leur généralisation, ayant permis d'aboutir à l'algorithme RSA
 
 \subsection{Le petit théorème de Fermat}
 \begin{thm}
 \normalfont
 Pour chaque paire d'entier  $a,$ $ b$ avec $b \ne 0$, il existe des entiers uniques $q,$ $r$ tel que $a = q b + r$ et $0 \le r < \left|{b}\right|$:
 \begin{center}
 \normalfont
 $\forall a, b \in \Z, b \ne 0: \exists! q, r \in \Z: a = q b + r, 0 \le r < \left|{b}\right|$.
 \end{center}
 \end{thm}
 Dans l'équation ci dessus:\\
 * $a$ est le $dividende$\\
 * $b$ est le $diviseur$\\
 * $q$ est le $quotient$\\
 * $r$ est le $reste$.
 \begin{lem}
 \normalfont
 Étant donné des entiers positifs $m$ et $n$, il est possible de choisir les entiers  $x$ et $y$ tel que $mx+ny=d$, ou $d=$pgcd $(m,n)$ est le  plus grand commun diviseur  entre $m$ et $n$.
 \end{lem}
 \begin{defn}
 \normalfont
 Si deux nombres $b$ et $c$ ont la propriété que leur différence $b-c$  est intégralement divisible par un nombre $m$ (i.e., $(b-c) / m$ est un nombre entier), alors $b$ et $c$ sont dit  "congrus modulo $m$." Le nombre $m$ est appelé le module, et la mention "$b$ est congru à $c$ (modulo $m$)" est écrit mathématiquement comme $b \equiv c \ \ $mod $ m$.
  \end{defn}
  On note $\frac{\mathbb{Z}}{p \mathbb{Z}}$  l'anneau des classes résiduelles modulo $p$ (avec $p$ un nombre premier). Donc pour $p$ premier, on a un corps. En algèbre, on appelle \emph{anneaux} \index{anneaux} certains ensembles munis d'une addition et d'une multiplication. Un \emph{corps} \index{corps} est une structure algébrique dans laquelle sont possibles les additions, soustractions, multiplications et calculs d'inverses multiplicatifs.  Le groupe multiplicatif des éléments inversibles est donc :
 \begin{center}
 \normalfont
$ (\frac{\mathbb{Z}}{p \mathbb{Z}})^*= \frac{\mathbb{Z}}{p\mathbb{Z}} \backslash\{0\}$.
\end{center}
Le petit théorème de Fermat concerne principalement tous les éléments \index{éléments}  inversibles de ce corps,
donc les classes non nulles. Un \emph{élément inversible} \index{élément inversible} est représenté par un entier non multiple de $p$.
\begin{thm}(Petit théorème de Fermat)
\normalfont
Soit $p$ un nombre premier et $a$ un entier naturel copremier  avec $p$ alors $a^{p-1}-1$ est divisible par $p$. En d'autres termes $a^{p-1} \equiv 1 \ \ $mod $ p.$
\end{thm}
Comme conséquence importante de ce théorème, on a la diminution de la taille des exposants    pour le calcul des puissances modulo $p$ :
\begin{thm}
\normalfont
Pour tout entier $a$ et $d$ avec $a$ copremier avec $p$ et  $d$ un exposant entier, on
obtient la relation suivante :
\begin{center}
\normalfont
$a^d \ \mod p = a^{d\ \ mod(p-1)} \ \ $mod $ p$.
\end{center}
\end{thm}
Le  \textbf{théorème 12} est une version du petit théorème de Fermat valable pour tout entier (comprenant les entiers multiples de $p$).
\begin{thm}
\normalfont
Pour tout entier $a$, on a la relation :
\begin{center}
\normalfont
$a^p \equiv a \ \ $mod $ p$.
\end{center}
\end{thm}
\begin{defn}
Pour $n > 1$, l'indicatrice ou le totient d'Euler est la fonction $\phi$, de l'ensemble $\mathbb{N}^*$ des entiers strictement positifs  qui à $n$ associe le nombre de nombres  premiers.
Plus formellement :
\begin{equation*}
\begin{array}{ccccl}
 \varphi&:&\N^*& \longrightarrow&\N^*\\ &&n&\longmapsto&\mathrm{card}(\{m\in\N^*~|~m\le n~\text{et}~m~\mathrm{premier~\grave a}~n\}).
\end{array}
\end{equation*}
Par exemple:
\begin{enumerate}
 \item $\phi(8) = 4$ car parmi les nombres de $1$ à $8$, seuls les quatre nombres $1, 3, 5$ et $7$ sont premiers à $8$. 
 \item $\phi(20) = 8$ car parmi les nombres de $1$ à $20$, seuls les quatre nombres $1 , 3 , 7 , 9 , 11 , 13 , 17$ et $19$ sont premiers à $20$.
\end{enumerate} 
\end{defn}
\subsection{Généralisation}
Soit maintenant un entier $n > 1$ quelconque. On étudie l’anneau $\frac{\mathbb{Z}}{n \mathbb{Z}}$ des classes résiduelles modulo
$n$. Nous notons $\frac{\mathbb{Z}}{n \mathbb{Z}}^*$  son sous-groupe multiplicatif des éléments inversibles. L'ordre de ce sous-groupe (son nombre d'éléments) est $\phi(n)$ où $\phi$ est la fonction indicatrice d'Euler. Alors :
\begin{thm}
\normalfont
Pour tout entier $a$ premier avec $n$, on a la relation :
\begin{center}
$a^{\phi{(n)}} \equiv 1 \ \ $mod $ n$.
\end{center}
\end{thm}
Ce théorème appliqué au cas précédent où $n$ est un nombre premier $p$ redonne le petit théorème de
Fermat.
\subsection{n= produit de deux nombres premiers}
Le cas où $n = pq$ est un produit de deux grands nombres premiers $p$ et $q$ est important en cryptographie puisqu'il permet de traiter le cas du système RSA. Dans ce cas, la fonction d'Euler est :
\begin{center}
$\phi(n)=(p-1)(q-1)$.
\end{center}
On a donc en conséquence du théorème précédent :
\begin{thm}
\normalfont
Pour tout entier $a$ premier avec $p$ et avec $q$ et tout entier $k$ on a la relation :
\begin{center}
$a^{k(p-1)(q-1)+1} \equiv a \ \ $mod $ n$.
\end{center}
\end{thm}
Cette dernière relation est à la base du système RSA. Elle nous permet de montrer comment se calcule le déchiffrement d’un texte chiffré.
\subsection{Le cryptage RSA}
Tous les outils mathématiques étant précédemment étudiés, nous parlons maintenant du cryptage RSA. RSA est un algorithme de cryptographie à clé publique utilisant la  factorisation de nombres premiers comme une fonction de trappe à sens unique. On définit:
\begin{center}
$n = pq$,
\end{center}
avec $p$ et $q$  des nombres premiers. Également, on définit   une clé privée $d$ et une clé publique $e$ telle que:
\begin{center}
$d.e \equiv 1 \ \ $mod $ \phi(n)$,\\
$(e,\phi(n))=1$,
\end{center}
où $\phi(n)$ est la fonction indicatrice d'Euler, $(e,\phi(n))$ désigne le plus grand commun diviseur ($(e,\phi(n))=1$ signifie que $e$ et $\phi(n)$ sont premiers).  

Supposons  que le message soit converti en  nombre $M$,  l'expéditeur crée  alors ses paires de clés  $n$ et $e$ et les rend publiques. Pour lui envoyer un message, on fait:
\begin{center}

$E=M^e \ \ $mod $ n$,
\end{center}
Pour décoder, le receveur (qui connait $d$) calcule:
\begin{center}
$E^d \equiv (M^e)^d \equiv M^{ed} \equiv M^{N( \phi(n)+1)} \equiv M \ \ $mod $ n$.
\end{center}
comme $N$ est un nombre entier, pour  casser le code, $d$ doit être trouvé. Mais cela nécessite la factorisation de $n$ comme
\begin{center}
$\phi(n)=(p-1)(q-1)$.
\end{center}
La sécurité du RSA repose sur le fait que, connaissant $n, e, $ et $ M^{e} \ \ $mod$ \ \ n$, il est très difficile de retrouver $M$ si l'on ne connait pas $d$. Retrouver $d$ consiste à factoriser  le produit $p \cdot q$. C'est pour cela que l'on dit que la sécurité de RSA repose sur la difficulté de factoriser de grands entiers en produits de deux facteurs premiers.
\section{Applicabilité à la cryptographie asymétrique}
En clair, la cryptographie à clé publique repose sur la notion de primalité. Cependant, comme on l'a vu avec RSA, beaucoup de notions élémentaires d'arithmétique telles que les congruences et l'arithmétique modulaire interviennent aussi.


Après avoir étudié la notion de primalité des figures discrètes et  décrit les notions d'arithmétiques intervenant dans la cryptographie asymétrique, dans cette section, on propose et oriente sur les approches permettant l'utilisation de certains outils d'arithmétiques, essentiels à l'application  de la cryptographie aux figures discrètes.
\begin{defn}
On note $\mathcal{H}$ l'ensemble des chemins auto-évitants qui restent dans le demi-espace
$\{ x_1>0 \}$ à partir de leur premier pas, et $h_N$ le cardinal des chemins de $\mathcal{H}$ à $N$ pas. On dit qu'un chemin auto-évitant à $N$ pas $\omega$ est un pont s'il vérifie
\begin{center}
$\forall i \in \|1,N \| 0< \omega_1(i) \leq \omega_1(N)$.
\end{center}
\end{defn}
\subsubsection{Polyominos et théorie des nombres}
\begin{enumerate}
\item \textbf{Notion d'addition}

Si $\omega$ et $\omega'$ sont deux chemins auto-évitants de longueurs respectives $N$ et $N'$, on assimile la notion d'addition comme leur
concaténation $(\omega \cdot \omega')$ donnant le chemin $W$ de longueur $\rho=N + N'$ (voir figure \ref{cheminaddmulti}) et  vérifiant.


\[\rho(i) = \left\{ 
\begin{array}{l l}
  \omega($i$) & \quad \text{pour $i\in \|0,N \|$ }\\
  \omega($N$)+ \omega'($i - N$) & \quad \text{pour $i\in \|N+1,N+N' \|.$}\\ \end{array} \right. \]



On remarque que  la concaténation de deux chemins auto-évitants n'en donne pas nécéssairement
un troisième. La concaténation de deux ponts en donne toujours un troisième.
\item \textbf{Notion de multiplication}

Si $w$ et $w'$ sont deux chemins auto-évitants de longueurs respectives $N$ et $N'$, on peut définir la notion de multiplication comme la composition de  $w$ et $w'$, dénotée par $\partial_w(w')$ (voir figure \ref{cheminaddmulti}).
\begin{figure}[tbph]
\begin{center}

\includegraphics[scale=0.4]{Logos/addmultipchemin}
\end{center}
\caption{Addition et multiplication de chemins}
\label{cheminaddmulti}
\end{figure}
\item \textbf{Relation d'ordre}

Soient $u, v$ deux mots. Il est facile de vérifier que " u est facteur de v " est une relation réflexive, antisymétrique et transitive et donc une relation d'ordre (par contre, ce n'est pas une relation d'ordre total). On introduit une restriction de cette relation sur les palindromes comme suit.
\begin{defn}
Soit  $P$ et $Q$ deux palindromes. On écrit $P \leq Q$ s'il existe un mot $x$ tel
que $q = xp\tilde{x}$ et on dit que $p$ est un \emph{facteur palindromique central} de $q$. La relation $p \leq q$, où $p$ et $q$ sont des palindromes, est un ordre partiel
(mais ce n'est pas un ordre total).
\end{defn}
\begin{defn}
Soit deux polyominos $P$ et $Q$, la relation d'ordre  $P \leq Q$ peut se définir comme la comparaison des longueurs $N$ et $N'$ des mots de contour ou des périmètres respectifs de $P$ et $Q$. Si $N \leq N'$ alors $P \leq Q$. La relation $P \leq Q$, où $P$ et $Q$ sont des polyominos, est un ordre partiel
(mais ce n'est pas un ordre total). 
\end{defn}
\item \textbf{Égalité }
\begin{defn}
Soit $\Sigma$ un alphabet ordonné, et $u, v\in \Sigma^{*}$ deux mots conjugués,  l’ordre lexicographique, noté $\leq$, est
défini par : $u \leq v$ si $u = xau'$, $v = xbv'$ avec $x, u', v' \in \Sigma,$ $a, b \in \Sigma^{*}$ et $a < b$.
\end{defn}
\begin{defn}
Soit $\Sigma$ un alphabet ordonné. Un mot primitif est un mot de Lyndon s’il est minimal
pour l’ordre lexicographique dans sa classe de conjugaison.
\end{defn}
\begin{defn}
Deux polyominos sont égaux si et seulement si leurs mots de Lyndon associés sont
égaux. L’intérêt de cette représentation par les mots de Lyndon est de pouvoir résoudre facilement le problème de l'égalité entre polyominos.
\end{defn}



\item \textbf{Notion de division et de multiple :}

Un polyomino \textbf{A} est dit diviseur d'un polyomino \textbf{B} si une copie de \textbf{B} 
peut être assemblée à partir de copies de \textbf{A} (pavage). 
On dit alors que \textbf{A} pave \textbf{B} (notion de $diviseur$), \textbf{B} est pavable par \textbf{A} (notion de divisibilité) donc  $multiple$ de \textbf{A}. Le monomino divise tout polyomino.
\item \textbf{Notion de commun diviseur et commun multiple :}

Nous avons respectivement(voir figure~\ref{numbertheory}) un monomino, un domino, deux trominos nommés \textbf{I} et 
\textbf{V}, et cinq tetrominos nommés \textbf{I}, \textbf{L}, \textbf{N}, \textbf{O} et \textbf{T}.
\begin{figure}[tbph]
\begin{center}

\includegraphics[scale=0.3]{Logos/numbertheory}
\end{center}
\caption{Polyominos}
\label{numbertheory}
\end{figure}
Un polyomino est dit être un commun diviseur de deux autres polyominos 
s'il  peut les paver. Un polyomino est dit le \textbf{plus grand diviseur commun} si aucun 
autre polyomino commun diviseur  ne peut paver avec moins de morceaux les deux autres polyominos. Notez que nous disons un grand commun 
diviseur plutôt que le plus grand commun diviseur, car un grand commun 
diviseur n'est pas nécessairement 
unique (voir figure~\ref{triominos}). Par exemple, les deux hexaminos ci-dessous ont à la fois le triomino \textbf{I} 
et  \textbf{V}  comme grands communs diviseurs.
\begin{figure}[tbph]
\begin{center}

\includegraphics[scale=0.3]{Logos/triominoes}
\end{center}
\caption{Grands communs diviseurs}
\label{triominos}
\end{figure}
\item \textbf{Polyominos et arithmétique modulaire:}

Il s'avère très important d'étudier la notion de congruence des polyominos puisque la cryptographie asymétrique  utilise les congruences sur les entiers et le petit théorème de Fermat pour obtenir des fonctions à sens unique,avec brèche secrète (ou porte dérobée). La notion de congruence est donc fondamentale. 

En arithmétique élémentaire, on utilise la division euclidienne pour la caractérisation du reste à l'aide de congruences:

Il nous faut donner des idées sur la notion de congruence des figures discrètes et la notion de reste. Rappelons qu'en arithmétique, la caractérisation du reste à l'aide de congruence se définit par  $r$ et le reste de la division euclidienne de $m$ par $n$: 
\[n \leftrightarrow  \left\{ 
  \begin{array}{l l}
    r \equiv {m[n]} & \quad \text{} \\
    0\leq r <|n| & \quad \text{} 
  \end{array} \right.\]
\textbf{Congruence.} Mathématiquement, deux formes sont congruentes si elles ont la même dimension et la même forme, indépendamment de leur orientation. Si vous pouvez saisir l'une des formes et la faire pivoter, la retourner et/ou la faire coulisser de façon à ce qu'elle se superpose exactement à une autre forme, alors les deux formes sont congruentes (voir polyomino $A,B,C,D,E,F,G,H$, figure \ref{congrue}). La concaténation de plusieurs formes congruentes résultent en une forme congruente (voir polyomino $FEHD$,  figure \ref{congrue}). Pour les polyominos, la congruence constitue le test à effectuer.
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.3]{Logos/congruencee}
\end{center}
\caption{Polyominos congruents}
\label{congrue}
\end{figure}

\textbf{Reste.} Les problèmes de combinatoire impliquant la notion géométrique de reste de polyominos semblent complexes. 
Il ne me semble  exister aucune règle ou formule connue permettant de déterminer le reste de polyominos après pavage ou division non complète. Intuitivement, le problème se pose comme dans la figure \ref{euclid}. Il s'agira de mettre en place des outils combinatoires capables de caractériser le reste d'une division de polyominos. De futures recherches sur les problèmes de combinatoire impliquant la notion géométrique de reste sont à faire.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.3]{Logos/division_euclidienne}
\end{center}
\caption{Comment combinatoirement caractériser le reste  d'une division euclidienne d'un polyomino?}
\label{euclid}
\end{figure}
\end{enumerate}



