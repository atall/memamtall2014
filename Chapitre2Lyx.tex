

\chapter*{Définitions et notation}

La terminologie et les notations introduites dans ce chapitre sont
tirées de de M. Lothaire, terminologies formelles nécessaires pour
l’étude de la combinatoire des mots (Lothaire, 1983)


\section{Mots et équations}


\subsection{Généralités sur les mots}

Un alphabet dans la combinatoire des mots est un ensemble fini non
vide de lettres \\(symboles, caractères). Nous appelons mot sur un
alphabet $\mathcal{A}$ toute séquence finie de lettre. Un mot d'un
alphabet sera généralement une simple juxtaposition de ses lettres.
Un mot fini $w$ est une fonction $w : [1,2, ..., n] \rightarrow \mathcal{A}$.
\begin{example}
Alphabet: ADN ( acides nucléiques, paires de bases, bases), ARN (
acides nucléiques), protéines (acides amines). 

$\mathcal{A}=\{a, y, b, x\} \implies $$\epsilon$, $x,a,xaxa $ sont
des mots de l'alphabet $\mathcal{A}$.

\newpage

\begin{figure}[tbph]


\includegraphics{Logos/adnalphabet}

\caption{Tétrade de 4 lettres de l'alphabet génétique: les trois lettres C,
G, A sont communs à l'ADN et l'ARN. Elles constituent la base triangulaire
de la tétrade. L'élément distant est le pole T où U qui forme ainsi
la tétrade alphabétique ADN où ARN}


\end{figure}

\end{example}
On dénote par $\mathcal{A}^{*}$ l'ensemble de tous les mots finis
sur l'alphabet $\mathcal{A}$. $\mathcal{A}^{+}$ est l'ensemble de
tous les mots finis non vides sur l'alphabet $\mathcal{A}$. L'ensemble
$\mathcal{A}^{+}$ est considéré ainsi comme un semi-groupe libre
sur $\mathcal{A}$ alors que $\mathcal{A}^{*}$ est appelé un monoïde
libre. Nous utilisons la notation $\mathcal{A}^{+}=\mathcal{A}^{*}-{\epsilon}$
ou $\epsilon$ est un mot vide. Le symbole $\epsilon$ ne peut donc
pas être une lettre, c'est à dire un élément de $\mathcal{A}^{*}$.
C'est donc un métasymbole désignant une séquence de $0$ lettres.
La longueur d'un mot $w \in $ $\mathcal{A}^{*}$ est définie comme
étant le nombre de lettres du mot $w$. Elle est notée $|w|$. On
note par $w[i]$ pour $i =1, 2 , ..., n$, la lettre du mot $w$ à
l'indice $i$ (la numérotation commence en 1). Pour $w \neq \epsilon$,
chaque indice $i=1, 2, ..., n$ est la position de sur $w$. Pour
un mot $w \in \mathcal{A}^{*}$ et un entier naturel $n \in \mathbb{N}$,
on définit la n-ième puissance de $w$ notée $w^{n}$. Il est commode
que $w^{0}$ soit égale à $\epsilon$. Pour chaque mot $w$, lorsque
$n>1$ est un nombre entier nous disons que $w^{n}$ est une puissance
propre de $w$ . 
\begin{fact}
$w=w[1]w[2]...w[n]$ d'où la définition élémentaire d'identité entre
deux mots quelconques $x$ et $y$: $w=y \Leftrightarrow |w| =|y|$ 
\end{fact}
L'image mirroir d'un mot $w \in \mathcal{A}^{*}$ est le mot $\bar{w}$
défini par $\bar{w}=w[n] w[n-1]... w[1]$. $\First{w} = w_{1}$ et
$\First{w} = w_{n}$ $w$ dénote respectivement la première et la
dernière lettre du mot $w$.
\begin{defn}
L'ensemble des lettres pour former un mot quelconque w est appelé
$alph(w)$.\end{defn}
\begin{example}
$alph(maammamammamam)={m,a}$\end{example}
\begin{defn}
La concaténation est une opération binaire sur l'ensemble $\mathcal{A}^{*}$,
associative, non commutative et admettant le mot vide comme neutre.
$\mathcal{A}^{*}$, muni de cette opération est un monoïde non commutatif. 
\end{defn}
Le produit où la concaténation de deux mots $w$ et $y$ est le mot
composé des lettres de $w$ suivies de lettres de $y$. On le note
par $w \cdot y$ où plus simplement $wy$. Le mot vide $\epsilon$ est
l'élément vide pour la concaténation.
\begin{defn}
Un mot $y$ est un facteur d'un mot $w$ s'il existe deux mots $u$
et $v$ tels que $w=uyv$. Un mot $y$ est un préfixe d'un mot $w$
s'il existe un mot $v$ tel que $w=yv$. Un mot $y$ est un suffixe
d'un mot $w$ s'il exite un mot $u$ tel que $w=uy$. Un facteur,
un préfixe, un suffixe où un sous mot $x$ de $y$ est qualifié de
propre si $x \neq y$. 
\end{defn}
On note respectivement alors $x \preccurlyeq_{fact} y$ , $x\prec_{fact} y$
, $x\preccurlyeq_{pref} y$, $x \prec_{pref} y$ , $ x \preccurlyeq_{suff} y$
, $x \prec_{suff} y$ , $ x \preccurlyeq_{smot} y$ , $x \prec_{smot} y$
lorsque x est un facteur, un facteur propre, un préfixe, un préfixe
propre, un suffixe, un suffixe propre, un sous mot, un sous mot propre
de $y$. $\preccurlyeq_{fact}, \preccurlyeq_{pref} , \preccurlyeq_{suff}, \preccurlyeq_{smot}$
sont des relations d'ordre. On note respectivement Fact($w$), Préf($w$), Suff($w$) et Smot($w$) 
l'ensemble des facteurs, préfixes, suffixes et sous mots de $w$.
\begin{defn}
On dénote par $u^{-1}w $ l'unique mot tel que $uu^{-1}w =w$.
\end{defn}
Donc $u^{-1}w$ est le mot obtenu à partir de $w$ en supprimant le
premier $u$. La notation $wu^{-1}$ est définie similairement pour
$u$ comme suffixe du mot $w$. On dénote par $|w|_{u}$ le nombre
d'occurences de $u$ dans $w$. Un mot $w$ est un sous-mot d'un mot
$y$ s'il existe $|n|+1$ mots $w_{0}, w_{1}, ..., w_{|n|}$ tel que
$y=w_{0}x_{0}w_{1}x_{1}w_{2}...w_{|x|-1}x_{|x|-1}$.
\begin{example}
$\textbf{"bnjr"}$ est un sous mot du mot $\textbf{"bonjour"}$ mais
pas un facteur. $\textbf{"njo"}$ est a la fois un facteur et un sous
mot du mot $\textbf{"bonjour"}$. $\textbf{"nnu"}$ n'est pas un sous
mot du mot $\textbf{"bonjour"}$ \end{example}
\begin{defn}
Deux mots $w$ et $w'$ de $\mathcal{A}^{*}$ sont conjugués ($w \equiv w'$
où $w \equiv_{x} w'$ ) s'il existe deux mots $u$ et $v$ dans $\mathcal{A}^{*}$
tels que $ w=uv$ et $w'=vu$. La relation de conjugaison $\equiv$
à une relation d'equivalence et la classe du mot $w$ est dénote par
$[w]$.\end{defn}
\begin{example}
$taata$ et $atata$ sont conjugués

$u=ta$

$v=ata$

On considére $w=algorithmique$. Le mot $x=algo$ est un préfixe de
$w$, $y=ue$ est un suffixe de $w$, et $z=rithmi$ est un facteur
de $w$\end{example}
\begin{defn}
Le mot $x$ est une sous-séquence  du mot $w$ s'il existe $|w|+1$
mots $y_{0}, y_{1}, ..., y_{w}$ tels que $w=y_{0}w_{1}y_{1}w_{2}y_{2}...y_{|w|-1}w_{|w|} y_{w}$

Sous-séquence  $\neq$ sous-mot !\end{defn}
\begin{example}
Toujours avec $w=algorithmique$. Les mots $ w = amie$ et $y = aoiie$
sont des sous-séquence s de $w$.\end{example}
\begin{defn}
Soit $w$ un mot non vide. Un entier $p$ tel que $0<p\leq |w|$ est
une période de $w$ si $w[i]=w[i+p]$  pour $0 \leq i \leq |w|-p-1$.
La période d'un mot non vide $w$ est la plus petite de ses périodes.
Elle est notée pér$(w)$\end{defn}
\begin{example}
$w=aataataa$

pér$(w)=3$\end{example}
\begin{defn}
Un mot non vide est primitif s'il n'est la puissance d'aucun n'autre
mot que lui-même.

$x \in A^{+}$ est primitif $\Leftrightarrow$ si $\exists u \in A^{*}$
et $n \in \mathbb{N}$ tel que $x=u^{n}\Rightarrow u=x$ et $n=1$\end{defn}
\begin{example}
$tatta$ est primitif 

$\epsilon,tatata=(ta)^{3}$ ne sont pas primitifs\end{example}
\begin{defn}
Un morphisme est une application $\varphi :\mathcal{A}^{*}\Rightarrow \textit{B}^{*}$
telle que que $\varphi(uv)=\varphi(u) \varphi(v)$ pour n'importe
quels $u,v \in \mathcal{A}^{*}$. 
\end{defn}
Il est par conséquent suffisant de connaitre l'action de $\varphi$ sur
les lettres de $\mathcal{A}$ pour l'étendre au monode libre $\mathcal{A}^{*}$.
On dit d'un mot $w$ qu'il est point fixe du morphisme $\varphi $
si $w = \varphi(w)$. Soit $\mathcal{A}^{*}\Rightarrow \mathcal{A}^{*}$
un morphisme sur un alphabet $\mathcal{A}$. On peut montrer qu'un
mot w dont la première lettre $\alpha$ est fixée si et seulement
si la première lettre de $\varphi(\alpha)$ est $\alpha$ (Allouche
et Shallit, $2003$, chapitre $7$). Dans ce cas, on écrit $\varphi^{n}(\alpha)$.
La notation est justifée par le fait que $\varphi^{n}(\alpha)$ est
un préfixe de $w$ pour tout entier $n \geq 0$. Notons que certains
mots finis peuvent etre point fixe d'un morphisme.
\begin{example}
Soient $\mathcal{A} = {a,b}$ et $\mu: \mathcal{A}^{*}\Rightarrow A$
le morphisme donné par $\mu(a) = ab et \mu(b) = ba$. Alors admet
exactement deux points fixes :

$\mu(t) = t = abbabaabbaababbabaababbaabbabaab$ 

$\mu(t) = \bar{t} = baababbaabbabaababbabaabbaababba $
\end{example}
Le morphisme $\varphi$ est non effaçant si ni $w$(a) ni $w$(b)
n'est un mot vide. La longueur d'un morphisme $\varphi$ est l'entier
$||\varphi|| = |\varphi(a)| + |\varphi(b)|$. Tout morphisme s'étend
de manière naturelle à $\mathcal{A}^{\infty}$.
\begin{example}
Considérons

$\varphi: \begin{array}{ccc} a &\rightarrow& ab \\ b &\rightarrow& a  \end{array} \bar{\varphi}: \begin{array}{ccc} a &\rightarrow& ba \\ b &\rightarrow& a  \end{array}$
\end{example}
Le premier est le morphisme de Fibonacci, et le deuxième est son image
miroir. Les nombres de fibonacci sont défini par $\mathcal{F}_{0}=0, \mathcal{F}_{1}=1 et \mathcal{F}_{n}=\mathcal{F}_{n-1}+\mathcal{F}_{n-2}$ pour
$n \geq 2$.
\begin{example}
Suite $A000045$ de l'OEIS 

\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline 
{\small{$\mathcal{F}_{0}$}} & {\small{$\mathcal{F}_{1}$}} & {\small{$\mathcal{F}_{2}$}} & {\small{$\mathcal{F}_{3}$}} & {\small{$\mathcal{F}_{4}$}} & {\small{$\mathcal{F}_{5}$}} & {\small{$\mathcal{F}_{6}$}} & {\small{$\mathcal{F}_{7}$}} & {\small{$\mathcal{F}_{8}$}} & {\small{$\mathcal{F}_{9}$}} & {\small{$\mathcal{F}_{10}$}} & {\small{$\mathcal{F}_{11}$}} & {\small{$\mathcal{F}_{12}$}} & {\small{$\mathcal{F}_{13}$}} & {\small{$\mathcal{F}_{14}$}}\tabularnewline
\hline 
\hline 
0 & 1 & 1 & 2 & 3 & 5 & 8 & 13 & 21 & 34 & 55 & 89 & 144 & 233 & 377\tabularnewline
\hline 
\end{tabular}

\newpage
\end{example}
Les mots de fibonacci sont defini par $f_{0}=\epsilon, f_{1}=b,f_{2}=a,f_{n}=f_{n-1}+f_{n-2}$ 
pour $n \geq 3$
\begin{example}
21 mots de Fibonacci de longueur 6:

\begin{tabular}{|c|c|c|c|c|c|c|}
\hline 
{\small{AAAAAA}} & {\small{AAAAAB}} & {\small{AAAABA}} & {\small{AAABAA}} & {\small{AAABAB}} & {\small{AABAAA}} & {\small{AABAAB}}\tabularnewline
\hline 
\hline 
{\small{AABABA}} & {\small{ABAAAA}} & {\small{ABAAAB}} & {\small{ABAABA}} & {\small{ABABAA}} & {\small{ABABAB}} & {\small{BAAAAA}}\tabularnewline
\hline 
{\small{BAAAAB}} & {\small{BAAABA}} & {\small{BAABAA}} & {\small{BAABAB}} & {\small{BABAAA}} & {\small{BABAAB}} & {\small{BABABA}}\tabularnewline
\hline 
\end{tabular}
\end{example}

\section{Polyominos, mots et Chemins}


\section{Polyominos}

Les polyominos sont un assemblage de n carrés (pixels) finis congrus
joint côté par côté sans trou. C’est une généralisation des dominos
donc un assemblage de n carrés finis, congrue, joint arêtes par arêtes
sans trou tel que chaque carré où pixel soit rattaché à la figure
par au moins un de ses côtés. Dans le domaine ludique, les polyominos
sont un morceau d’échiquier.

\begin{figure}[tbph]
\begin{center}

\includegraphics[scale=0.1]{\string"Logos/Tetris Light\string".eps}

\end{center}

\caption{Polyominos, morceaux d’échiquier.}
\end{figure}


Une grille carré est un ensemble $\mathbb{Z}^{2}$.Une cellule est
une unite carré dans $\mathbb{R}^{2}$ dont les coins sont des points
de $\mathbb{Z}^{2}$. Nous dénotons par $c(i,i)$ la cellule :$c(i,j)=\{(x,y)$$ \in \mathbb{R}^{2} |$
$i\leq x\leqq i+1,j\leq y\leq j+1\}$. L'ensemble de toutes les cellules
est dénoté par $C$ 
\begin{defn}
Deux cellules $c$ et $d$ $\in$ $C$ sont appelées 4 voisins si
elles ont exactement un côté en commun donc se touchent par une arête.
\end{defn}
\begin{center}
\begin{example}
La cellule $P$ composante 4-voisins (nommée cellule $P2,P4,P6$ et
$P8$) : 
\end{example}
\begin{figure}[tbph]
\includegraphics[width=8cm,height=8cm,keepaspectratio]{Logos/4voisin}

\caption{4-voisins du pixel P }


\end{figure}


\end{center}

Clairement chaque cellule \foreignlanguage{english}{$c(i,j)$} a exactement
4 voisins connexe $c(i+1;j),c(i;j+1),c(i-1;j),c(i;j-1)$.
\begin{defn}
On appelle chemin connexe entre deux points $P_{0}$ et $P_{n}$,
une suite de points ( $P_{0}, P_{1} , P_{2} , P_{3} ,….. P_{n-1} , P_{n}$ )
tels que $P_{ i-1}$ et $P_{i}$ soient adjacents.
\end{defn}
\begin{figure}[tbph]
\begin{center}

\includegraphics{\string"Logos/chemin connexe\string".eps}

\end{center}

\caption{Chemin connexe}
\end{figure}

\begin{defn}
Une région 4 connexe de $\mathbb{Z}^{2}$ est tout sous-ensemble $R$
de $\mathbb{R}^{2}$  tel que pour tout $c,d$ $\in\mathbb{R}$ ,
il existe les cellules $c_1,c_2,...,c_k$ tel que $c=c_1$, $d=c_k$
, et $c_i,c_{i+1}$ sont 4 voisins pour $i=1,2,..,k-1$. Soit $P$
et $Q$ deux points de l’image et soit $S$ un sous-ensemble de pixels
contenant $P$ et $Q$, on dit que P et Q sont connectés s'il existe
un chemin connexe inclus dans $S$ entre $P$ et $Q$. $S$ est une
composante connexe si quelque soient $P$ et $Q$ $\in$ $S$, ils
sont connectés.
\end{defn}
\begin{figure}[tbph]


\caption{Exemples de composantes connexes (4 voisins)}


\begin{center}

\includegraphics[scale=0.6]{Logos/composante4connexe}

\end{center}
\end{figure}


\begin{figure}[tbph]
\caption{Exemples de composantes non-connexes (4 voisins)}


\begin{center}

\includegraphics[scale=0.6]{Logos/composantenonconnexe}

\end{center}
\end{figure}


Les isométries usuelles (translation, rotation et réflexion) sont
naturellement définies en région 4-connexe. Dans certains cas, il
est commode de considérer deux régions équivalentes à des isométries.
Par exemple, la relation entre $R$ $\sim_{\theta}$$S$ défini par
\textquotedbl{} $R$ est une copie translatée de $S$\textquotedbl{}
est une relation d'équivalence. Un polyomino simple est toute classe
d'équivalence $\sim_{\theta}$. Dans le même esprit, si la relation
$R$ $\sim_{\rho}$ est définie par \textquotedbl{}$R$ est une copie
translatée et rotationnée de $S$\textquotedbl{}, alors un polyomino
unilatérale est defini par toute classe d'équivalence  $\sim_{\rho}$$S$.
Finalement , tout polyomino libre est toute classe équivalence de
la relation $R$ $\sim_{\rho}$$S$ défini par \textquotedbl{} $R$
est une copie translatée, rotationnée, réfléchie de $S$ \textquotedbl{}. 

Une autre notion intéressante est la notion de trou. Donnée une région
$R$, on dénote le complément $\bar{R}$ i.e : $\bar{R}=C-R$. On
dit que $R$ est sans trou si $\bar{R}$ est une région $4$ connexe.

\begin{figure}[tbph]
\includegraphics{\string"Logos/free polyminoes\string".eps}

\caption{Tous polyominos libre de $c$ cellules, $c=1,2,3,4,5,6$ }
\end{figure}



\section{Mot de contour}


\subsection{Codage de freeman}

Le code de la chaîne Freeman $\mathcal{F}={0, 1, 2, 3}$ est considéré
comme un groupe additif modulo $4$. C’est la méthode la plus ancienne
de description des contours d'images et aussi la plus utilisée encore
aujourd’hui {[}Freeman, 1961, Freeman, 1977{]}. Si elle est moins
utilisée en reconnaissance des formes qu’elle le fut, elle est très
fréquemment utilisée dans des applications récentes comme la transmission
des images par zones (par exemple dans $MPEG${[}{]}). C’est donc
une technique de représentation des directions du contour (on code
la direction le long du contour dans un repère absolu lors du parcours
du contour à partir d’une origine à donner). Les directions peuvent
se représenter en 4-connexe où en 8-connexe. Le codage d’un contour
se fait donc de la facon suivante : 
\begin{enumerate}
\item transmission des coordonnées absolues du point de départ
\item transmission de la liste des codes de déplacement d’un point du contour
au suivant sur le maillage.
\end{enumerate}
Les codes des contours sont donnés par la figure ci-dessous

\begin{figure}[tbph]
\begin{center}

\includegraphics[scale=0.8]{Logos/4connexe8connexe}

\end{center}

\caption{Code de Freeman en 4-connexe (à gauche) et en 8-connexe(à droite).}


\end{figure}


\begin{figure}[tbph]
\begin{center}

\includegraphics{Logos/courbex_y}

\end{center}

\caption{Codage de Freeman. Approximation de la courbe A par la chaîne $x$
(en 4-connexe) et par la chaîne $y$ (en 8-connexe).}


\end{figure}



\section{Mots de contour}

Dans de nombreuses situations, il est commode de représenter un polyomino
par son mot de contour , qui à son tour est facilement représenté
par un mot sur un alphabet de Freeman par les étapes élémentaires:
$0 : \leftarrow$, $2 :\rightarrow$, $1 :\uparrow$,$3 :\downarrow$ .
La représentation des polyominos par son mot de contour offre les
caractéristiques intéressantes suivantes: 
\begin{enumerate}
\item Elle est simple et unique, à conjugaison et orientation prés.
\item Dans de nombreux cas, l'aire d'un Polyomino est quadratique par rapport
à son périmètre, de sorte que le mot de contour, qui est de longueur
égale au périmètre, occupe un espace raisonnable.
\item Toute la théorie issue de la combinatoire des mots peut être appliquée.
\item De nombreuses propriétés de symétrie et de convexité d'un polyomino
donné se détectent facilement en étudiant simplement son mot de contour.
\item Inversion d’un chemin : on inverse tous les descripteurs et on inverse
la séquence . L’inverse d’un descripteur $j$ est $\bar{j}=n/2+j mod(n)$.
exemple en 4-connexe $X=00110011 \bar(X)=22332233$
\item Fermeture d’un contour : on teste la fermeture d’un contour en vérifiant
que la chaıne réduite est nulle. On ferme un contour en lui ajoutant
le chemin inverse d’un de ces chemins réduits. Il y a  beaucoup d’autres
fermetures possibles que par addition de l’inverse d’un réduit, mais
les fermetures obtenues ainsi sont de longueur minimale. Il y a d’autres
fermetures minimales que celles obtenues par addition de l’inverse
d’un réduit
\item Réduction d’un chemin : c’est l’un des chemins de longueur minimale
reliant les 2 extrémités de la courbe initiale. On associe $2$ par
$2$ des descripteurs inverses de la chaıne et on les supprime exemple
en 4-connexe. On obtient tous les chemins réduits en changeant l’ordre
des associations.
\end{enumerate}
\begin{figure}[tbph]
\begin{center}

\includegraphics[scale=0.6]{Logos/exempledechemis}

\end{center}

\caption{Exemples de chemins. (a) Un chemin fermé qui n'est pas autoévitant,
(b) un chemin auto-évitant qui n'est pas fermé et (c) un mot de contour,
qui est fermé et autoévitant.}


\end{figure}



\subsection{Transformation sur les chemins}

Un chemin $p$ est un mot sur $\mathcal{F}$, c'est-a-dire un élément
de $\mathcal{F}$. On dit de $p$ qu'il est fermé si $|p_{0}| =|p_{2}| et |p_{1}| = |p_{3}|$.
Un sous-chemin $q$ de $p$ est simplement un facteur de$p$. De plus,
$p$ est dit auto-évitant s'il ne possède aucun sous-chemin fermé,
à l'exception du chemin lui-même et du chemin vide. Un mot de contour
est un chemin auto-évitant et fermé. 

Une transformation de chemins est une fonction $\varphi:\mathcal{F}^{*} \rightarrow \mathcal{F}^{*}$ 
transformant tout chemin $w$ en $\varphi(w)$. Nous définissons ci-dessous
les isométries naturelles du plan discret ( rotation multiple $\pi/2$ et
symétries préservant $\mathbb{Z}\times \mathbb{Z}$ ).\\$\textbf{Rotations.  }$ 
Le morphisme $\rho:\mathcal{F}^{*}\rightarrow \mathcal{F}^{*}$ appliqué
sur les lettres par 

\begin{center}

$\rho:x \rightarrow x+1$ mod 4

\end{center}

agit sur le chemin comme une rotation d'angle $\pi/2$
\begin{fact}
Le morphisme $\rho^{i}$, obtenu par compositon agit sur les chemins
comme une transformation d'angle $\frac{i\pi}{2}$. \end{fact}
\begin{defn}
Une rotation $\rho^{2}$ est notee $\overline{\cdotp}$, i.e. $\rho^{2}(0)=\overline{0}=2, \rho^{2}(1)=\overline{1}=3, \rho^{2}(2)=\overline{2}=0, \rho^{2}(3)=\overline{3}=1$ 

\begin{figure}[tbph]
\includegraphics[scale=0.43]{Logos/rotation}

\caption{Le chemin $w = 00121211232$ et ses rotations $\rho(w)$,$\rho^{2}(w)$
et $\rho^{3}(w)$.}


\end{figure}
$\textbf{Symétries.  }$  Soit le morphisme $\sigma_i:\mathcal{F}^{*} \rightarrow \mathcal{F}^{*}$
appliqué sur les lettres par 
\end{defn}
\begin{center}

$\sigma_i:x \rightarrow i-x$ mod 4

\end{center}

Donc $\sigma_0, \sigma_1,\sigma_2,\sigma_3$ agissent respectivement
sur les chemins par symétrie par rapport aux axes $y=0$, $x=y$,
$x=0$  et $x=-y$.

\begin{figure}[tbph]
\includegraphics[scale=0.43]{Logos/symetrie}

\caption{Le chemin $w = 00121211232$ et les symétries $\sigma_{0}(w)$,$\sigma^{1}(w)$
et $\sigma^{2}(w)$.}


\end{figure}
$\textbf{Parcours inversé. }$  Considérons l'antimorphisme $\widehat{\cdotp}:\mathcal{F}^{*} \rightarrow \mathcal{F}^{*}$
défini par

\begin{center}

$\widehat{\cdotp}=\rho^2 \circ \tilde{\cdotp}=\tilde{\cdotp} \circ \rho^2$

\end{center}

La transformation $\widehat{\cdotp}$ sur un chemin $w$ inverse le
sens du parcours

\begin{figure}[tbph]
\includegraphics[scale=0.43]{Logos/inverse}

\caption{Les chemins homologues $(a)$ $w = 00121211232$ et $(b)$ $\widehat{w}= 01033030322$.}
\end{figure}



\subsection{virage}

Le code de Freeman permet de décrire des chemins selon les quatre
directions élémentaires haut, bas, gauche, droit. Il existe une autre
représentation pratique des chemins discrets basée sur la notion de
virages. À cette fin, nous introduisons l'opérateur $\bigtriangleup$
sur $\mathcal{F}$, qui est en fait une extension à un alphabet de
$4$ lettres de celui présenté au chapitre $2$. Plus précisément,
soit $w = w_{1}w_{2}...w_{n} \in \mathcal{F}$ un chemin de longueur
$n\geq 2$. Le mot des différences finies $\Delta(w) \in F^{*}$ de
$w$ est défini par :

\begin{center}
$\Delta(w) = (w_{2}-w_{1}).(w_{3}-w_{2}).(w_{n}-w_{n-1}).$
\end{center}


D'un point de vue géométrique, les lettres $\0,\1,\2$ et $\3$ correspondent
respectivement au mouvement aller en avant , tourner à gauche , reculer
et tourner à droite . La figure ci dessous illustre l'effet de l'opérateur
$\Delta$ sur le chemin $w = \0\1\0\1\2\2\2\3\2\1\1$.

\begin{figure}[tbph]
\caption{Interprétation géométrique de l'opérateur sur le chemin $w = \0\1\0\1\2\2\2\3\2\1\1$.
Le mot $\Delta(w)=\1\3\1\1\0\0\1\3\3\0$ décrit les virages effectués
pour parcourir le chemin w selon la correspondance suivante : $\0$ :
$\textit{aller en avant}$, $\1$ : $\textit{tourner à gauche}$
et $\3$ : $\textit{tourner à droite}$. Comme le chemin est auto-évitant,
il n'y a pas de lettre $\2$ : $\textit{reculer}$.}
\end{figure}


Il est utile de définir une opération qui inverse en quelque sorte
l'effet de l'opérateur $\Delta$. Soit $w = w_{1}w_{2}...w_{n}$ un
mot de longueur $n\geq 1$ et $\alpha \in \mathcal{F}$ une lettre.
Le mot des sommes partielles $\sum_{\alpha}(w)$de $w$ à partir $\alpha$ est
donné par

\begin{center}
$\sum_{\alpha}(w)= (\alpha).(\alpha+w_{1}).(\alpha+w_{1}+w_{2})...(\alpha+w_{1}+w_{2}+...+w_{n}).$
\end{center}

L'opérateur $\Delta$ vérifie la propriété suivante, facile à démontrer:
\begin{prop}
Soient $u = u_{1}u_{2}...u_{m},v = v_{1}v_{2}...v_{n} \in \mathcal{F}^{*}$
des mots de longueur $m$, $n\geq 2$ respectivement. Alors $\Delta(uv) = \Delta(u)\Delta(u_{n}v_{1})\Delta(v)$.
\end{prop}
Lorsqu'on considère des chemins qui ne sont pas auto-évitant, il est
possible qu'ils contiennent certains facteurs de l'ensemble $\mathcal{R} = \{ \0\2, \1\3, \2\0, \3\1 \}$,
c'est-à-dire l'ensemble des chemins de longueur $\2$ dont les pas
sont de directions opposées. Néanmoins, il est toujours possible de
réduire un mot $w \in \mathcal{F}^{*}$ en un unique mot $w'$ pour
qu'il ne contienne pas de tels facteurs : il suffit de supprimer recursivement
toutes les occurrences de facteurs de $\mathcal{R}$ dans $w$. Remarquons
par contre que le mot réduit n'est pas forcement auto-évitant.
\begin{example}
Le mot $w = \1\0\0\2\1$ se réduit au mot $w = \1\0\1$ après suppression
du facteur $\0\2$. Le mot $u = \1\1\1\3\3\3$ se réduit au mot vide
$\epsilon$ après avoir supprimé recursivement trois fois le facteur
$\1\3$ :
\end{example}
\begin{center}
$\1\1\1\3\3\3 \rightarrow\1\1\3\3\rightarrow\1\3 \rightarrow \epsilon$\\
\end{center}


\section{Génération pseudo-aléatoire de polyminos partant d'une image binaire}

À faire dès que possible...
\end{document}
