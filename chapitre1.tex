\chapter{Définitions et notations} \label{chap2}

Dans ce chapitre, on introduit la notation et des définitions relatives à la combinatoire des mots et  aux polyominos. Le lecteur intéressé à en savoir davantage est référé à M. Lothaire \cite{lothaire1983combinatorics}.

\section{Mots} \label{sec:mots}

Un \emph{alphabet} \index{alphabet} $A$ est un ensemble fini dont les éléments sont appelés \emph{lettres} (ou \emph{symboles}). Nous appelons \emph{mot} \index{mot} sur un alphabet $A$ toute suite finie de lettres. Plus formellement, un \emph{mot fini} \index{mot fini} $w$ sur un alphabet $A$ est une suite finie $(w_1, w_2,...,w_n)$ d'éléments de $A$, où $n \in \N$. Afin d'alléger la notation, on écrit $w = w_1 w_2 \cdots w_n$. Pour $i = 1,2,\ldots,n$, on note par $w[i]$ la lettre du mot $w$ à l'indice $i$ (souvent, pour des raisons arithmétiques, on commence l'indexation par $0$). L'entier $n$ est appelé la \emph{longueur} de $w$, notée $|w|$. Il existe un unique mot $w$ tel que $|w| = 0$. Ce mot est appelé \emph{mot vide} et est noté $\epsilon$.

\begin{example}
Soit $A = \{a, y, b, x\}$ un alphabet. Alors $\epsilon$, $x$, $a$ et $xaxa$ sont des mots sur l'alphabet $A$.
\end{example}

On désigne par $A^{n}$ l'ensemble des mots de longueur $n$ sur $A$, où $n \in \N$. L'ensemble des mots de longueur quelconque sur $A$, noté par $A^{*}$ est défini par:
\begin{center}
$A^{*}=\bigcup_{n \geq 0} A^{n}.$
\end{center}

Soient deux mots $u = u_1 u_2 \cdots u_m$ et $v = v_1 v_2 \cdots  v_m$ sur $A$, avec $m,n \in \N$. On appelle \emph{concaténation} \index{concaténation} de $u$ et $v$ le mot $u \cdot v = u_1 u_2 \cdots u_m v_1 v_2 \cdots v_n $. Souvent, on écrit simplement $uv$ plutôt que $u \cdot v$. La concaténation de deux mots est une opération associative sur $A^{*}$, de sorte que $(A^{*}, \cdot)$ est un monoïde, appelé \emph{monoïde libre}, dont l'élément neutre est $\epsilon$.

Soit $w$ et $y$ deux mots sur l'alphabet $A$. On dit que $y$ est un \emph{facteur} \index{facteur} de $w$ s'il existe deux mots $u$ et $v$ tels que $w = uyv$. En particulier, si $u = \varepsilon$, alors $y$ est appelé \emph{préfixe} \index{préfixe} de $w$ et si $v = \varepsilon$, alors on dit que $y$ est un \emph{suffixe} \index{suffixe} de $w$. Un facteur, un préfixe ou un suffixe $y$ de $w$ est dit \emph{propre} \index{propre} si $x \neq \varepsilon, y$. Ces notions induisent naturellement des relations sur les mots. On écrit $x \preceq_{\emph{fact}} y$, $x \prec_{\emph{fact}} y$, $x \preceq_{\emph{pref}} y$, $x \prec_{\emph{pref}} y$, $x \preceq_{\emph{suff}} y$ et $x \prec_{\emph{suff}} y$ si $x$ est respectivement un facteur, un facteur propre, un préfixe, un préfixe propre, un suffixe ou un suffixe propre de $y$. Les relations $\preceq_{\emph{fact}}$, $\preceq_{\emph{pref}}$, $\preceq_{\emph{suff}}$ sont des relations d'ordre partiel. On note respectivement $\Fact(w)$, $\Pref(w)$ et $\Suff(w)$ d'un mot $w$ donné l'ensemble des facteurs, préfixes et suffixes. 

\begin{example}
Soit $w=algorithmique$. Le mot $algo$ est un préfixe de $w$, $que$ est un suffixe de $w$ et $rithmi$ est un facteur de $w$.
\end{example}

Soit $w$ un mot indicé entre $1$ et $n$. On dit que le nombre $i$ est une \emph{occurrence} \index{occurence} de $u$ si $w = xuy$ pour certains mots $x$ et $y$, avec $|x| = i - 1$. On désigne par $|w|_u$ le nombre d'occurrences de $u$ dans $w$. En outre, $u$ est dit \emph{unioccurrent} \index{unioccurrent} dans $w$ si $|w|_u= 1$.


La \emph{n-ième puissance} \index{n-ième puissance} d'un mot non vide $w$, notée $w^n$, est donnée par $w^{n}= ww\cdots w$ ($n$ fois). On dit que $ w$ est \emph{primitif} \index{primitif} s'il n'existe aucun mot $u$ tel que $w = u^{n}$, pour un certain entier $n$. En particulier, le \emph{carré} \index{carré} de $w$ est donné par $w^2$.

\begin{example}
$(aba)^{3}=abaabaaba$ et $aba^{3}=abaaa$. 
\end{example}

\begin{example}
Le mot $abaab$ est primitif. Les mots $\epsilon$ et $ababab=(ab)^{3}$ ne sont pas primitifs.
\end{example}

Deux mots $u$ et $v$ de $A^{*}$ sont \emph{conjugués}\index{conjugués}, et on écrit $u \equiv v$ s'il existe deux mots $x$ et $y$ tels que $u = xy$ et $v = yx$. La relation de conjugaison $\equiv$ est une relation d'équivalence et la classe du mot $w$ est dénotée par $[w]$. On dit alors que $[w]$ est un \emph{mot circulaire}\index{mot circulaire}.

\begin{example}
Les mots $u = ababa$ et $v = abaab$ sont conjugués. En effet, il suffit de prendre $x = ab$ et $y = aba$ et on voit que $u = ababa = xy$ et $v = abaab = yx$.
\end{example}

L'\emph{image miroir} \index{image miroir} d'un mot $w \in A^{*}$ est le mot $\widetilde{w}$ défini par $\widetilde{w}=w[n] w[n-1]... w[1]$. Autrement dit, l'ordre des lettres est renversé. On dénote par $\First{w}$ et $\Last{w}$ respectivement la première et la dernière lettre du mot $w$. L'ensemble des lettres qui apparaissent dans un mot $w$ est noté $\Alp(w)$. Par exemple, $\Alp(maammamammamam) = \{m,a\}$.

Soit  $\varphi :A^{*}\rightarrow B^{*}$ une fonction, où $A$ et $B$ sont deux alphabets.  On dit que $\varphi$ est un  \emph{morphisme} \index{morphisme} s'il préserve la concaténation, c'est-à-dire que $\varphi(uv)=\varphi(u) \varphi(v)$, pour n'importe quels $u,v \in A^{*}$. D'autre part, on dit que $\varphi$ est un \emph{antimorphisme} \index{antimorphisme}  si $\varphi(uv)=\varphi(v) \varphi(u)$ pour n'importe quels $u,v \in A^{*}$. L'application image miroir  $\: \widetilde{\cdot} \:$ est un antimorphisme.

Soit la fonction $f:A \rightarrow A$, une involution est une application bijective telle que $f \circ f=id_A$. Supposons que $A = \{a,b\}$. Le \emph{complément} \index{complément} d'un mot $w \in A^{*}$, dénoté par $\bar{w}$, est le mot obtenu par l'application du morphisme échangeant les lettres de $w$, c'est-à-dire que  $\: \bar{\cdot} \:$  est le morphisme défini par $\bar{a} = b$ et $\bar{b} = a$. Il est clair que l'opération de complémentation est une involution.

La complexité (\emph{factorielle}\index{factorielle}) d'un mot est la fonction indiquant, pour chaque naturel $n$, le nombre de mots de longueur $n$, c'est-à-dire la fonction
\begin{center}
$C_{w}(n) : \mathbb{N} \rightarrow \mathbb{N} : n \mapsto |Fact_n(w)|.$ 
\end{center}

\section{Polyominos}

Les polyominos sont des assemblages de $n$ carrés (pixels) finis congrus
joints côté par côté sans trou. C’est une généralisation naturelle des dominos (voir figure~\ref{polyomino}).

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.075]{\string"Logos/tetrislight\string".png}
\end{center}
\caption{Un pavage partiel utilisant des polyominos de $4$ cellules, aussi appelés tétraminos.}
\label{polyomino}
\end{figure}


La \emph{grille carrée} \index{grille carré} est l'ensemble $\mathbb{Z}^{2}$. Une cellule est
une unité carré dans $\mathbb{R}^{2}$ dont les coins sont des points
de $\mathbb{Z}^{2}$. Nous dénotons par $c(i,j)$ la cellule $c(i,j)=\{(x,y)$$ \in \mathbb{R}^{2} |$
$i\leq x\leq i+1,j\leq y\leq j+1\}$. L'ensemble de toutes les cellules
est dénoté par $C$.
\begin{defn}
Deux cellules $c$ et $d$ $\in$ $C$ sont dites \emph{$4$-adjacentes} \index{$4$-adjacence} si elles ont exactement une côté en commun, c'est-à-dire si elles se touchent par une arête (voir figure~\ref{voisines}).
\end{defn}

\begin{figure}[tbph]
\includegraphics[width=8cm,height=8cm,keepaspectratio]{Logos/4voisin}
\caption{Les quatre cellules $4$-adjacentes ($P_2,P_4,P_6,P_8$) de la cellule $P$.}
\label{voisines}
\end{figure}

Clairement chaque cellule $c(i,j)$ a exactement quatre cellules voisines $c(i+1,j)$, $c(i,j+1)$, $c(i-1,j)$ et $c(i,j-1)$.
\begin{defn}
On appelle \emph{chemin $4$-connexe} \index{chemin $4$-connexe} (voir figure~\ref{chconx}) entre deux cellules $c$ et $d$ toute suite de cellules $(c_1,c_2,\ldots,c_n)$ telle que $c = c_1$, $d = c_n$ et $c_i$ et $C_{i+1}$ sont $4$-adjacentes pour $i=1,2,\ldots,n-1$. Un ensemble de cellules $E$ est dit $4$-connexe si, pour toute paire de cellules $c,d \in E$, il existe un chemin $4$-connexe entre $c$ et $d$ n'utilisant que des cellules de $E$. Un ensemble connexe est parfois appelé \emph{région $4$-connexe} ou simplement \emph{région}.
\end{defn}
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.9]{\string"Logos/chemin connexe\string".png}
\end{center}
\caption{Chemin 4-connexe}
\label{chconx}
\end{figure}

La figure~\ref{rgcont} illustre des ensembles $4$-connexes et la figure \ref{rgncon} illustre des exemples d'ensembles non connexes.
\begin{figure}[tbph]
\caption{Deux ensembles connexes.}
\label{rgcont}
\begin{center}
\includegraphics[scale=0.55]{Logos/composante4connexe}
\end{center}
\end{figure}


\begin{figure}[tbph]
\caption{Deux ensembles non connexes (les cellules de même couleur ne sont pas toutes $4$-connexes.}
\label{rgncon}
\begin{center}
\includegraphics[scale=0.55]{Logos/composantenonconnexe}
\end{center}
\end{figure}

Les isométries usuelles (translation ($\theta$), rotation ($\rho$) et réflexion ($\sigma$)) sont naturellement définies pour les régions $4$-connexe. Dans certains cas, il est commode de considérer deux régions équivalentes à isométrie près. On écrit $R \sim_{\theta} S$ si $S$ est une copie translatée de $R$. Il s'agit clairement d'une relation d'équivalence. Dans le même esprit, on écrit $R \sim_{\rho} S$ si $S$ peut être obtenu de $R$ par une combinaison de translations et de rotations. Finalement, on écrit $R \sim_{\sigma} S$ si $S$ est obtenu de $R$ par une combinaison de translations, de rotations et de réflexions. Les classes d'équivalence des relations $\equiv_\theta$, $\equiv_\rho$ et $\equiv_\sigma$ sont appelées respectivement \emph{polyomino fixe}, \emph{polyomino unilatéral} et \emph{polynomino libre}\index{polyomino fixe}\index{polyomino unilatéral}\index{polyomino libre}. Les polyominos libres sont représentés à la figure \ref{polylibre}.

Une autre notion importante est celle de trou. Soit $R$ une région. On définit le \emph{complément} \index{complément} de $R$ par $\bar{R} = C - R$. Un \emph{trou} de $R$ est une région $T \subseteq \bar{R}$ qui est $4$-connexe, maximale et finie. Si $R$ est une région finie, alors elle peut avoir un nombre fini de trous et exactement une région de $\bar{R}$ est infinie.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.9]{\string"Logos/free polyminoes\string".png}
\caption{Tous les polyominos libres de $c$ cellules, pour $c\in \{1,2,3,4,5,6\}$ }
\label{polylibre}
\end{center}
\end{figure}

\section{Mot de contour}

Un alphabet fondamental en géométrie digitale est le code de Freeman $\mathcal{F}=\{\0, \1, \2, \3\}$. Pour des raisons pratiques, il est convenable de le considérer comme un groupe additif modulo $4$\index{codage de Freeman}. Il permet de coder un chemin le long du contour dans un repère absolu à partir d’une origine fixée. Il peut y avoir $4$ ou $8$ directions permises selon la situation (voir figure~\ref{48direct}). 

Le codage d’un contour se fait donc de la façon suivante : 
\begin{enumerate}
\item On indique les coordonnées absolues du point de départ.
\item On donne la liste des codes de déplacement d’un point du contour au suivant dans l'espace discret.
\end{enumerate}
La figure \ref{approxfreeman} nous donne un exemple d'approximation d'une courbe par le code de Freeman.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.65]{Logos/4connexe8connexe}
\end{center}
\caption[Code de Freeman en $4$-connexe (à gauche) et en $8$-connexe (à droite)]{Code de Freeman en $4$-connexe (à gauche) et en $8$-connexe (à droite) \url{<http://perso.telecom-paristech.fr/~tupin/TDI/poly_formes.pdf>}}
\label{48direct}
\end{figure}


\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.65]{Logos/courbex_y}
\end{center}
\caption[Code de Freeman. Approximation de la courbe $A$ par la chaîne $x$
(en $4$-connexe) et par la chaîne $y$ (en $8$-connexe)]{Code de Freeman. Approximation de la courbe $A$ par la chaîne $x$
(en $4$-connexe) et par la chaîne $y$ (en $8$-connexe) \url{<http://perso.telecom-paristech.fr/~tupin/TDI/poly_formes.pdf>}}
\label{approxfreeman}
\end{figure}

Dans ce mémoire, nous nous concentrons sur l'alphabet de Freeman $\Freeman$ de $4$ lettres. Un chemin (voir figure~\ref{cheminex}) $p$ est un mot sur $\mathcal{F}$, c'est-à-dire un élément de $\mathcal{F}$.
\begin{figure}[tbph]
\begin{center}

\includegraphics[scale=0.6]{Logos/exempledechemis}

\end{center}

\caption[Exemples de chemins]{Exemples de chemins. (a) Un chemin fermé qui n'est pas auto-évitant, (b) un chemin auto-évitant qui n'est pas fermé et (c) un mot de contour
qui est fermé et auto-évitant.}
\label{cheminex}
\end{figure}

Un chemin $p$ est un mot sur $\Freeman$, c'est-à-dire un élement de $\Freeman^{*}$. On dit de $p$ qu'il est \emph{fermé} \index{fermé} si $|p|_{0} =|p|_{2}$ et $|p|_{1} = |p|_{3}$. Un sous-chemin $q$ de $p$ est simplement un facteur de $p$. Le chemin  $p$ est dit \emph{auto-évitant} s'il ne possède aucun sous-chemin propre fermé. Un \emph{mot de contour} est un chemin auto-évitant et fermé. 

Dans de nombreuses situations, il est commode de représenter un polyomino sans trou par son mot de contour, qui à son tour, est facilement représenté par un mot sur l'alphabet de Freeman. La représentation des polyominos par mot de contour offre les caractéristiques intéressantes suivantes: 
\begin{enumerate}
  \item Elle est simple, unique  à conjugaison et orientation près.
  \item Dans la plupart des cas, l'aire d'un polyomino est quadratique par rapport
à son périmètre. Conséquemment cela fait que la représentation du  contour, de longueur égale  à la longueur du périmètre, occupe ainsi un espace plus compact.
  \item Les principes et théories issus de la combinatoire des mots peuvent y être appliqués.
  \item De nombreuses propriétés de symétrie, d'isométrie et de convexité d'un polyomino donné sont détectées facilement par l'étude et l'analyse simple du  contour. Par exemple, si le contour est fermé et auto-évitant, on le définit comme un mot de contour.
\end{enumerate}

Les isométries de base de la grille discrète (rotations d'angle multiple de $\frac{\pi}{2}$ et réflexions) se définissent naturellement à l'aide de morphismes appliqués sur les lettres.

\textbf{Rotations.} Le morphisme $\rho:\mathcal{F}^{*}\rightarrow \mathcal{F}^{*}$ défini par
$$\rho : x \mapsto (x+1) \bmod 4,$$
agit sur le chemin comme une rotation d'angle $\frac{\pi}{2}$. Par conséquent, le morphisme $\rho^{i}$ est une d'angle $\frac{i\pi}{2}$.

La rotation d'angle $\pi$ (voir figure~\ref{rotation}) $\rho^{2}$ est aussi notée $\:\overline{\cdotp}\:$, c'est-à-dire la rotation définie par $\rho^{2}(0)=\overline{0}=2, \rho^{2}(1)=\overline{1}=3, \rho^{2}(2)=\overline{2}=0, \rho^{2}(3)=\overline{3}=1$.
\begin{center}
\begin{figure}[tbph]
\includegraphics[scale=0.45]{Logos/rotation}
\caption{Le chemin $w = \0\0\1\2\1\2\1\1\2\3\2$ et ses rotations $\rho(w)$, $\rho^{2}(w)$
et $\rho^{3}(w)$.}
\label{rotation}
\end{figure}
\end{center}

\textbf{Symétries.} Étant donné $i \in \{0,1,2,3\}$, soit le morphisme $\sigma_i:\mathcal{F}^{*} \rightarrow \mathcal{F}^{*}$ défini par
$$\sigma_i : x \rightarrow (i - x) \bmod 4.$$

Donc $\sigma_0, \sigma_1,\sigma_2,\sigma_3$ agissent respectivement sur les chemins par symétrie (voir figure~\ref{symetrie}) par rapport aux axes $y = 0$, $x = y$, $x = 0$ et $x = -y$.
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.45]{Logos/symetrie}
\end{center}
\caption{Le chemin $w = \0\0\1\2\1\2\1\1\2\3\2$ et les symétries $\sigma_{0}(w)$, $\sigma_{1}(w)$ et $\sigma_{2}(w)$.}
\label{symetrie}
\end{figure}

\textbf{Parcours inversé.} Rappelons qu'un antimorphisme sur un alphabet $A$ est une application provenant de la fonction miroir $\varphi:A^{*}\rightarrow A^{*}$ vérifiant $ \varphi(uv)=\varphi(v)\varphi(u)$ pour n'importe quels $u,v \in A^{*}$. On dit d'un antimorphisme qu'il est involutif si $\varphi^2$ = $\Id$. Il est facile de vérifier que tout antimorphisme involutif $\varphi$ peut-être décomposé en $\varphi=\sigma \circ R = R \circ \sigma$ où $\sigma$ est une involution sur l'alphabet $A$.
\label{antimorphismeeeee} 
L'antimorphisme $\:\widehat{\cdotp}\::\mathcal{F}^{*} \rightarrow \mathcal{F}^{*}$ est défini par
$$\: \widehat{\cdotp} \: = \rho^2 \circ \:\tilde{\cdotp}\:=\:\tilde{\cdotp}\: \circ \rho^2.$$
D'un point de vue géométrique, l'antimorphisme $\: \widehat{\cdot} \:$ correspond à inverser la direction de parcours d'un chemin (voir figure~\ref{parcinv}). On dit alors que $w$ et $\hat{w}$ sont des chemins \emph{homologues}.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.6]{Logos/inverse}
\end{center}
\caption{Les chemins homologues $(a)$ $w = \0\0\1\2\1\2\1\1\2\3\2$ et $(b)$ $\widehat{w}= \0\1\0\3\3\0\3\0\3\2\2$}
\label{parcinv}
\end{figure}

\section{Virages et enroulements}

Comme mentionné auparavant, le code de Freeman décrit des chemins selon les quatre directions élémentaires \emph{haut}, \emph{bas}, \emph{gauche}, \emph{droit}. Cependant, il existe une autre représentation pratique des chemins discrets basée sur la notion de virages. À cette fin, nous introduisons l'opérateur $\Delta$ sur $\mathcal{F}$ de la façon suivante. Soit $w = w_{1}w_{2}...w_{n}$ un chemin de longueur $n \geq 2$. Le \emph{mot des différences finies} $\Delta(w) \in F^{*}$ de $w$ est défini par
$$\Delta(w) = (w_{2}-w_{1})\cdot(w_{3}-w_{2})\cdot(w_{n}-w_{n-1}),$$
où la soustraction est prise modulo $4$.

D'un point de vue géométrique, les lettres $\0$, $\1$, $\2$ et $\3$ correspondent respectivement au mouvement \emph{aller en avant}, \emph{tourner à gauche}, \emph{reculer} et \emph{tourner à droite}. La figure ~\ref{op} illustre l'effet de l'opérateur $\Delta$ sur le chemin $w = \0\1\0\1\2\2\2\3\2\1\1$.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.6]{Logos/virage}
\end{center}
\caption[Interprétation géométrique de l'opérateur sur le chemin $w = \0\0\1\2\1\2\1\1\2\3\2$]{Interprétation géométrique de l'opérateur sur le chemin $w = \0\0\1\2\1\2\1\1\2\3\2$.
Le mot $\Delta(w)=\0\1\1\3\1\3\0\1\1\3\0$ décrit les virages effectués
pour parcourir le chemin $w$ selon la correspondance suivante : $\0$ :
$\textit{aller en avant}$, $\1$ : $\textit{tourner à gauche}$
et $\3$ : $\textit{tourner à droite}$. Comme le chemin est auto-évitant,
il n'y a pas de lettre $\2$ : $\textit{reculer}$.}
\label{op}
\end{figure}

L'opérateur $\Delta$ vérifie la propriété suivante, facile à démontrer:
\begin{prop}
Soient $u = u_{1}u_{2}...u_{m},v = v_{1}v_{2}...v_{n} \in \mathcal{F}^{*}$ des mots de longueur $m$, $n\geq 2$ respectivement. Alors $\Delta(uv) = \Delta(u)\Delta(u_{n}v_{1})\Delta(v)$.
\end{prop}

Afin d'alléger l'écriture de certaines démonstrations, nous étendons la fonction $\Delta$ aux couples de mots de la façon suivante. Soient $u$ et $v$ des mots non vides. Alors on écrit $\Delta(u,v) = \Delta(\Last{u} \First{v})$.

Lorsqu'on considère des chemins non auto-évitants, il est possible que ceux-ci contiennent certains facteurs de l'ensemble $\mathcal{R} = \{ \0\2, \1\3, \2\0, \3\1 \}$, c'est-à-dire l'ensemble des chemins de longueur $\2$ dont les pas sont de directions opposées. Il est alors possible de réduire un mot $w \in \mathcal{F}^{*}$ en un unique mot $w'$ pour qu'il ne contienne pas de tels facteurs : on supprime alors récursivement toutes les occurrences de facteurs de $\mathcal{R}$ dans $w$. On remarque par contre que le mot réduit n'est pas forcément auto-évitant.
\begin{example}
Le mot $w = \0\2\0\2\1\3\2\0\3\1$ se réduit à $w' = \0\0\1\2\3$ après suppression des facteurs $\0\2$, $\1\3$, $\2\0$, $\3\1$. Par contre, $w'$ n'est pas auto-évitant.
\end{example}
\begin{example}
Le mot $w = \0\1\0\1\3$ est  réduit à $w = \0\1\0$ après suppression du facteur $\1\3$.  Le mot $u = \1\1\3\3\0\0\2\2$ est réduit au mot $\0\0\2\2$ après la suppression  recursive de  deux fois le facteur $\1\3$. Par la suite  $\0\0\2\2$ est réduit au mot vide
$\epsilon$ après la suppression récursive de deux fois le facteur $\0\2$. Autrement dit, on a les réécritures suivantes~:
\end{example}
$$\1\1\3\3\0\0\2\2 \rightarrow\1\3\0\0\2\2\rightarrow\0\0\2\2 \rightarrow \0\2\rightarrow \epsilon.$$

Il est utile de définir une opération qui inverse en quelque sorte l'effet de l'opérateur $\Delta$. Soit $w = w_{1}w_{2}...w_{n}$ un mot de longueur $n\geq 1$ et $\alpha \in \mathcal{F}$ une lettre. Le \emph{mot des sommes partielles} $\sum_{\alpha}(w)$de $w$ à partir $\alpha$ est donné par
$$\sum_{\alpha}(w)= (\alpha)\cdot(\alpha+w_{1})\cdot(\alpha+w_{1}+w_{2})\cdots(\alpha+w_{1}+w_{2}+...+w_{n}).$$

Le \emph{nombre d'enroulements} \index{nombre d'enroulements} d'une courbe fermée dans le plan autour d'un point donné est un nombre entier représentant la totalité de fois que la courbe se déplace dans le sens antihoraire autour du point donné. Il dépend donc de l'orientation de la courbe. 

La notion de nombre d'enroulements est fondamentale dans l'étude de la topologie algébrique. Elle joue aussi un rôle important en calcul vectoriel, en analyse complexe, en géométrie différentielle et en physique, y compris dans la théorie des cordes. Sur l'alphabet de Freeman, cette notion est très simple à définir et il est également possible de l'étendre aux courbes non fermées. La figure \ref{nbreroulement} illustre deux enroulements autour d'un point $p$.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.5]{Logos/enroulement}
\end{center}
\caption{Illustration de la notion de nombre d'enroulements:  deux enroulements autour du point $p$}
\label{nbreroulement}
\end{figure}

Sur la grille discrète, cela revient à compter les virages à gauche et à droite effectués, alors que les mouvements en avant ne modifient pas le nombre d'enroulements. Les mouvements de recul sont cependant un peu plus complexes à traiter. Plus formellement, soit $w \in \Freeman^{*}$ un chemin et $w'\in \Freeman^{*}$ son chemin réduit après suppression récursive des facteurs dans $ \{ \0\2, \1\3, \2\0, \3\1\}$.  On définit le \emph{nombre d'enroulements} \index{nombre d'enroulements} de $w$ par
\begin{center}
$\mathcal{T}(w)= \frac{|\Delta{(w')}|_{1}-|\Delta{(w')}|_{3}}{4}.$
\end{center}

\begin{prop}
Soient $u = u_{1}u_{2}...u_{m},v = v_{1}v_{2}...v_{n} \in \mathcal{F}^{*}$ des mots de longueur $m$, $n\geq 2$ respectivement. Alors $\mathcal{T}(uv)=\mathcal{T}(u)\mathcal{T}(u_{n}v_{1})\mathcal{T}(v)$.
\end{prop}

Comme pour le mot des différences
finies, il est pratique  de calculer le nombre d'enroulement du mot de longueur $2$ qui consiste de
la dernière lettre de $u$ et de la première lettre de $v$. Ainsi, on écrit

\begin{center}
$\mathcal{T}(u,v) = \mathcal{T}(\Last{u} \First{v})$.
\end{center}

Comme le mot de contour d'un polyomino n'est pas unique, il est pratique de représenter
un chemin fermé $w$ par sa classe de conjugaison $[w]$, qu'on appelle aussi \emph{mot circulaire}\index{mot circulaire}.

Si $w$ est un chemin fermé, alors un ajustement est nécessaire à la fonction $\mathcal{T}$ puisqu'on prend en compte le virage entre la dernière et la première lettre du mot $w$. Le \emph{mot des premières différences} \index{mot des premières différences} d'un mot circulaire $[w]$ est défini par :
\begin{center}
$\Delta{([w])}=\Delta{([w]) \cdot(w_1-w_n)}.$
\end{center}

 Il est  possible de réduire un mot circulaire $[w]$ en un mot circulaire $[w']$ en supprimant récursivement les pas de directions opposées de l'ensemble $ \{ \0\2, \1\3, \2\0, \3\1\}$. En appliquant la même règle, un mot circulaire $[w]$ est circulairement réduit a un mot unique $[w']$. Il est alors possible d'étendre naturellement la définition de \emph{nombre d'enroulements d'un mot circulaire} \index{nombre d'enroulements d'un mot circulaire} par
 \begin{center}
 $\mathcal{T}([w])= \frac{|\Delta{([w'])}|_{1}-|\Delta{([w'])}|_{3}}{4}.$
 \end{center} 
 
 Il est bien connu que le nombre d'enroulements d'un mot de contour est contraint~:
\begin{thm}
(\textbf{Daurat et Nivat, 2003}) Soit $[w]$ un mot de contour circulaire. Alors
le nombre d'enroulements de $[w]$ est $\mathcal{T} ([w]) =1$   si le
parcours est orienté dans le sens anti-horaire et $\mathcal{T} ([w])=-1$ si le
parcours est orienté dans le sens  horaire.
\end{thm}

La fonction $\mathcal{T}$ ainsi que les isométries $\rho^i$ $(i \in \{ 0, 1, 2, 3\})$ vérifient les propriétés suivantes :
\begin{prop}
Soit $w \in \Freeman^{*}$ un chemin.
\begin{enumerate}
  \item $\mathcal{T}(\rho^i(w))=\mathcal{T}(w)$;
  \item $\mathcal{T}(\sigma^i(w))=-\mathcal{T}(w)$;
  \item  $\mathcal{T}(\hat{w})=-\mathcal{T}(w)$.
\end{enumerate}
\end{prop}

\begin{proof}
Soit $w$ un chemin et $w'$ son chemin réduit associé.
\begin{enumerate}
\item Le résultat suit du fait que $\Delta(\rho^i(w))=\Delta(w)$.
\item Il suffit d'observer que la réflexion $\sigma_i$ inverse les virages à gauche avec les virages à droite et laisse inchangés les pas vers l'avant et les pas vers l'arrière.
\item L'antimorphisme $\:\hat{\cdot} \:$ inverse les lettres \0 et \2 ainsi que les lettres \1 et \3. Par conséquent
$$\mathcal{T}(\hat{w})= \frac{|\Delta{(\hat{w})}|_{3}-|\Delta{([\hat{w}])}|_{1}}{4}
= \frac{|\Delta{(w)}|_{1}-|\Delta{([\hat{w}])}|_{3}}{4}
=- \mathcal{T}(w),$$
tel que voulu.
\end{enumerate}
\end{proof}

\section{Représentation des polyominos à partir d'une image binaire}


Lorsque nous manipulons des régions discrètes, il en existe plusieurs modes de représentations possibles. Pour les polyominos, en plus de la représentation par mot de contour, nous pouvons également utiliser des matrices binaires (voir figure~\ref{etiquette}). Elle présente l'avantage de pouvoir décrire des régions non connexes et avec trou, ce qui est moins naturel dans le cas des mots de contour. Pour un polyomino quelconque, on identifie un pixel par le bit $0$ pour signifier qu'il est vide et par le bit $1$ pour signifier qu'il est occupé.
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.8]{Logos/image_binaire}
\end{center}\caption{Étiquetage de composantes connexes}
\label{etiquette}
\end{figure}

L'étiquetage en composantes connexes est donc généralement présent lorsqu’il faut regrouper des pixels connexes. Il est à la base de la plupart des chaines algorithmiques en traitement d'images analysant des régions dans une scène. Plusieurs types d'algorithmes standard sont utilisés pour coder l'information et leur efficacité varie suivant la nature des données. On trouvera ci-dessous une description sommaire de quelques techniques courantes.


L’approche pixel est une approche de comparaison de l'étiquette courante
avec les étiquettes du voisinage (voir figure \ref{approchepixel}).

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=1.1]{Logos/approchepixel}
\end{center}\caption{Étiquetage 4-connexe et 8-connexe}
\label{approchepixel}
\end{figure}
Le codage par arbre quaternaire est une  méthode de représentation permettant de minimiser l'information nécessaire pour coder des zones homogènes. C'est une forme de compactage qui divise l'image en quatre quadrants (voir figure \ref{quadtree}). Chaque quadrant est à son tour divisé en 4 sous quadrants et ainsi de suite.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.6]{Logos/quadtreee}
\end{center}\caption{Arbre quatrenaire}
\label{quadtree}
\end{figure}

En balayant l’image ligne par ligne, il est possible d'appliquer l’encodage de plage sur toutes les séquences de points qui ont la même couleur. Le codage par longueur de plage (RLC) est utilisé pour rechercher des segments adjacents pour déterminer la connectivité des segments. Pour réduire la complexité algorithmique et le rendre plus compacte, le codage par arbre quaternaire est généralement associé à RLC.

Cependant ces approches utilisent un grand nombre de tests. Ce qui se traduit au niveau du  processeur par une importante consommation de ressource  \cite{lacassagne1998real}. 



Nous proposons de représenter de façon compacte une image par une matrice binaire,  à l'aide d'une liste d'entiers, où chaque ligne (ou, de façon équivalente, chaque colonne) correspond à la représentation binaire de l'entier en question (voir figure~\ref{case}). Par exemple, pour des tétraminos, on se limite à des entiers (voir figure~\ref{tetraminoes}) allant de $1$ à $15$.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.8]{Logos/examplerepresentation}
\end{center}
\caption{Pour ce polyomino (1 représentant une case occupée, le
0 représentant une case vide), chaque ligne  correspond donc respectivement aux
entiers 9, 13 et 7.}
\label{case}
\end{figure}

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.7]{\string"Logos/polyomino 4 cellules\string".png}
\end{center}
\caption{Tous les polyominos de 4 cellules}
\label{tetraminoes}
\end{figure}
