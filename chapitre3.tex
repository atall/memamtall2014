\chapter{Exploration informatique }

Dans ce chapitre, nous nous intéressons à l'évaluation pratique des deux algorithmes de factorisation décrits au chapitre précédent. Nous expliquons de quelle façon un banc d'essai est généré, puis nous proposons une analyse comparative de nos deux algorithmes de factorisation des polyominos.

\section{Méthologie }
 
\subsection{Java }

Pour l'évaluation expérimentale, notre choix s'est porté sur le langage de programmation \textbf{Java}. Afin de visualiser les objets construits, nous avons choisi le langage de programmation \textbf{Python} avec son module graphique \og \textbf{Turtle} \fg{}, très adapté pour illustrer les figures discrètes. L'interface entre les deux langages s'est faite par l'intermédiaire du langage Jython, qui est un interpréteur Python écrit en Java.

Pour chronométrer les différents temps de calcul, nous utilisons la bibliothèque  \textbf{java.lang.} \textbf{management}, qui offre la fonction $currenttime()$ (voir algorithme~\ref{current}), c'est-à-dire l'heure du système actuel qui est déterminée immédiatement avant et après l'exécution de l'algorithme. On atténue ainsi l'impact des tâches de traitement de fond sur le temps de fonctionnement. La valeur retournée en minutes, secondes, millisecondes ou nanosecondes représente le temps fixe d'exécution.

Typiquement, pour une étude comparative de la complexité en temps, la plupart des programmes sont codés similairement en introduisant des fragments de code ressemblant à ce qui se trouve à l'algorithme \ref{current}.
\begin{figure}[tbph]
\includegraphics[width=15cm]{Logos/CurrentTimemillis}
\caption{Algorithme \textquotedbl{}currentTimeMillis()\textquotedbl{} }
\label{current}
\end{figure}

\subsection{Métriques d'évaluation}

La notation \emph{grand-O} \index{grand O} décrit le comportement d'une fonction lorsque l'argument tend vers l'infini, généralement à l'aide de fonctions simples (voir par exemple la référence classique \cite{cormen2001introduction} pour plus de détails).

Lorsqu'on évalue les performances d'un algorithme de façon empirique, on doit utiliser différentes mesures pour estimer le comportement asymptotique du temps d'exécution. On suppose dans ce chapitre que tous les temps de calcul suivent une règle de puissance de la forme $p \sim kn^a$.

Il est alors possible d'estimer la valeur du coefficient $a$ en prenant deux temps d'exécution $t1$ et $t2$ ainsi que les deux tailles des instances $n1$ et $n2$, qui doivent vérifier la relation
$$\frac{t_2}{t_1} = \left(\frac{n_2}{n_1}\right)^a.$$
On peut alors facilement estimer $a$ à l'aide de la formule
$$a=\log\left(\frac{t2}{t1}\right)/ \log\left(\frac{n2}{n1}\right).$$
Voir \cite{goldreich2008computational} pour plus de détails sur l'estimation empirique de la complexité.

Nous nous intéressons donc à estimer \emph{l'ordre de complexité local} par rapport à la notation grand-O. Nous nous attendons donc à obtenir une borne près de $\BigO(n^7)$ pour l'algorithme naïf et $\BigO(n^5)$ pour l'algorithme amélioré. Si l'ordre de complexité local suit une règle de puissance quelconque alors la valeur empirique doit rester constante en fonction des données en entrée. %Puisque la notation grand O mesure l'ordre de complexité de l'algorithme, cela dépend donc d'une fonction telle que pour une certaine entrée $n$, on a la fonction $f(n) \cdot c$ qui procure une borne supérieure du temps d'exécution de l'algorithme.  En d'autres termes, pour une entrée $n \geq n_{0}$ et une constante c, l'ordre de complexité ne dépasse jamais $f(n) \cdot  c$. Empiriquement cela se manifeste par le fait que l'ordre de complexité local pour un $ n \geq n_{0}$ est constant donc suit une règle de puissance. On aura atteint alors une borne empirique. Dans le cas contraire, $a$ change drastiquement selon les données en entrée. 

Considérant la puissance de calcul de l'ordinateur et la méthode d'implémentation des algorithmes (style du codage, techniques utilisées par le développeur, langages de programmation), la borne empirique peut être très différente de la borne théorique. C'est pourquoi, il est plus important de retenir que l'évaluation empirique de $a$ servira toujours d'indicateur de comparaison locale de la performance de nos deux algorithmes de factorisation même si une borne théorique n'est pas atteinte \cite{greene2007mathematics}. 

Pour l'évaluation empirique, on prend  comme paramètres le temps d'exécution et l'ordre de complexité local de nos deux algorithmes de factorisation. Nous testons donc sur un très grand nombre de jeux de données nos deux algorithmes de factorisation en fonction de la longueur du mot de contour. 

%\subsection{Ordonnancement}
%
%L’analyse empirique se base sur l’exécution de l’algorithme. Cette analyse est utile puisqu’elle permet~: 
%\begin{enumerate}
%\item d'établir  une hypothèse sur la complexité empirique locale des algorithmes;
%\item de comparer la complexité algorithmique des deux algorithmes de factorisation en pratique.
%\end{enumerate}
%
%Le processus d'analyse consiste à :
%\begin{enumerate}
%\item Établir l’objectif de l’analyse;
%\item Choisir une métrique efficace (nombre d’exécution des opérations au niveau du CPU ou le temps d’exécution de l’algorithme); 
%\item Décider des caractéristiques des données d’entrées; 
%\item Implémenter l’algorithme dans un langage de programmation;
%\item Générer les données d’entrées;
%\item Exécuter le programme selon les données d’entrées et enregistrer les résultats; 
%\item Analyser les résultats obtenus.
%\end{enumerate}


\section{Banc d'essai }

À l'instar des graphes (orientés et non orientés), l'énumération de polyominos est un problème très difficile et il semble très peu probable qu'il existe un algorithme efficace permettant de tirer de façon complètement aléatoire un polyomino de périmètre $p$, pour une valeur de $p$ donnée.

Nous devons donc nous rabattre sur une stratégie de génération qui ne garantit par l'uniformité. Nous avons choisi de générer $250$ polyominos premiers de longueur de mot de contour allant de $10$ à $200$ et $1000$ polyominos composés de longueur de mot de contour allant de $100$ à $900$. Les polyominos premiers sont générés pseudo-aléatoirement. Les polyominos composés sont générés sur la base de polyominos premiers, auxquels on applique un morphisme parallélogramme $\varphi$ lui aussi généré de façon aléatoire.

À cet effet, nous développons deux générateurs aléatoires :
\begin{enumerate}
  \item un générateur pseudo-aléatoire de polyominos, que nous dénotons par GPoly;
  \item un générateur pseudo-aléatoire de polyominos parallélogrammes, que nous dénotons par GPara.
\end{enumerate}

\subsection{Génération de polyominos}

Il s’avère nécessaire de concevoir un générateur de polyominos premiers. En effet, on ne peut énumérer les polyominos de façon manuelle ou déterministe puisqu'il y en a un nombre dont la croissance est exponentielle. Notre banc d'essai est donc pseudo-aléatoire à défaut d'être complètement aléatoire. 

Il y a différentes façons de résoudre le problème de la construction du générateur. Il convient de commencer par la solution la plus intuitive. L'idée de base de cette méthode simple est la suivante~: étant donné un polyomino $P$, soit un générateur aléatoire $G$ qui prend en entrée un nombre entier $n$ qui correspond au nombre de cellules à générer des polyominos.

\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.8]{Logos/generateurpoly}
\caption{Ajout d'une cellule aléatoire à un polyomino.}
\label{gen}
\end{center}
\end{figure}

Comme le montre la figure \ref{gen} ci-dessus, on construit un polyomino de $n$ cellules en lui ajoutant au hasard une cellule sur son contour. Cette opération est effectuée jusqu'à ce qu’on ait placé le bon nombre de cellules. L'ensemble de cellules ainsi généré est clairement connexe.

Clairement, un polyomino ainsi généré peut être premier ou composé, mais, dans la plupart des cas, il s'agit d'un polyomino premier. Pour cette raison, nous proposons un deuxième générateur dans la sous-section suivante, qui nous permet de construire un échantillon de polyominos composés.

\begin{algorithm}
\small
\setstretch{0.5}
\SetKwFunction{Contour}{Contour}
\SetKwFunction{carrunitaire}{carrunitaire}
\Deb{
$NbrePolyominos=ALEATOIRE(40)$ \tcc{Recursivement générer un nombre $x$ de polyominos possédant chacun un nombre $y$ de cellules}  
$cellules =ALEATOIRE(Gamme~saisie~utilisateur)$ \\
\tcc{Entrez la portée  du nombre de cellules à générer}
\Pour{$Polyominos_Générés  \leftarrow 1$ \bf{\`{a}} $|NbrePolyominos|$}{ 

\Pour{$i \leftarrow 1$ \bf{\`{a}} $|cellules|$}{ 

$chemin=ALEATOIRE(\leftarrow , \rightarrow ,  \uparrow  , \downarrow)$  \\
\Si(){$chemin==\rightarrow $}
{
\Contour($\rightarrow$).\carrunitaire($\uparrow,\leftarrow,\downarrow,\rightarrow $)
}
\SinonSi{$chemin==\uparrow$}
{
\Contour($\uparrow$).\carrunitaire($\uparrow,\leftarrow,\downarrow,\rightarrow $)
}
\SinonSi{$chemin==\leftarrow$}
{
\Contour($\leftarrow$).\carrunitaire($\uparrow,\leftarrow,\downarrow,\rightarrow $)
}
\SinonSi{$chemin==\downarrow$}
{
\Contour($\downarrow$).\carrunitaire($\uparrow,\leftarrow,\downarrow,\rightarrow $)
}
}

}
}

\bf{Fonction} \Contour{int $chemin$ )}{} \tcc{Extension aléatoire des arêtes :$\leftarrow, \rightarrow,  \uparrow, \downarrow$}
\Deb{
$x_{cell}=0$\\
$y_{cell}=0$\\
\Si(){$chemin==\rightarrow $}{$x_{cell}+=10$} 	
\SinonSi{$chemin==\uparrow$}{$y_{cell}+=10$} 	    
\SinonSi{$chemin==\leftarrow$}{$x_{cell}-=10$} 	    
\SinonSi{$chemin==\downarrow$}{$y_{cell}-=10$}
$SetPos(x_{cell},y_{cell})$\\
}


\bf{Fonction} \carrunitaire{($\uparrow,\leftarrow,\downarrow,\rightarrow $ )}{} \tcc{Construire un carré unitaire pour chaque extension} 
\Deb
{
$int [] carré~unitaire=[\uparrow,\leftarrow,\downarrow,\rightarrow ]$  
\Pour{$i \leftarrow 1$ \bf{\`{a}} $|4|$}
{ 
\Si(){$carré~unitaire[i]==\rightarrow $} 		    	
{
  $heading(90)$
  $forward(10)$
}
\SinonSi{$carré~unitaire[i]==\uparrow$} 		    	
{
  $heading(360)$
  $forward(10)$
}
\SinonSi{$carré~unitaire[i]==\leftarrow$} 		    	
{
  $heading(270)$
  $forward(10)$
}
\SinonSi{$carré~unitaire[i]==\downarrow$} 		    	
{
  $heading(180)$
  $forward(10)$
}
}

}
\caption{Générateur pseudo-aléatoire de polyominos}\label{alg:gprem} 
\end{algorithm}

\DecMargin{1em}

\selectlanguage{french}%




\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.7]{Logos/gpremfigure}

\caption{GPoly. Banc d'essai disponible dans l'archive électronique jointe comme matériel complémentaire.}
\end{center}
\end{figure}

\subsection{Génération de polyominos composés}

Rappelons qu'un polyomino non unitaire $P$ est appelé \emph{composé} s'il existe un mot de contour quelconque $u$ et un morphisme parallélogramme quelconque $\varphi$ tel que $P=\varphi(u)$. En d'autres termes, s'il peut être décomposé comme le produit d'un morphisme parallélogramme et d'un autre polyomino. Nous nous chargeons de générer pseudo-aléatoirement le polyomino parallélogramme $\varphi$ puisque le polyomino composé est construit par $P=\Poly(\varphi)$ et que l'algorithme de génération de polyominos premiers se charge de générer le mot de contour quelconque $u$  de $P=\Poly(\varphi)$.

Notre algorithme (voir algorithme~\ref{alg:gpara}) commence en générant les morphismes homologues qui remplacent par leur chemin arbitraire le pas élémentaire horizontal $\varphi(\0)$ et vertical $\varphi(\1)$. La longueur des chemins $\varphi(0)$ et $\varphi(1)$ est spécifiée selon des paramètres. Nous nous assurons que le morphisme est homologue et que le polyomino est parallélogramme. Nous nous assurons également que les chemins arbitraires verticaux et horizontaux sont auto-évitants et fermés.

Plus précisément, nous suivons les étapes ci-bas~:
\begin{enumerate}
  \item Soit $n$ le nombre de polyominos parallélogrammes à générer;
  \item Soit la longueur aléatoirement générée $\ell$;
  \item Pour $i\in [1,n]$, nous construisons respectivement les chemins  $y_{i}$ et $z_{i}$ tel que $y_i= \varphi_{i}(\0)$ et $z_i= \varphi_{i}(\1)$;
  \item Pour $i\in [1,n]$, nous construisons $w_{i}'=y_{i} \cdot z_{i}$;
  \item Nous créons l'image du chemin inverse $\varphi(\overline{w'_i})$;
  \item Nous vérifions que le résultat est auto-évitant.
\end{enumerate}

\selectlanguage{english}%
\IncMargin{1em}
\begin{algorithm}[H]
\small
\setstretch{0.5}
\SetKwFunction{MorphismeGen}{MorphismeGen}
\SetKwFunction{ferme+auto-evitant}{ferme+auto-evitant}
\Deb{
$Polyomino~para=1$\\
\Pour{$Polyomino~para \leftarrow 1$ \bf{\`{a}} $|ALEATOIRE(Gamme~saisie~utilisateur)|$}{ 
\MorphismeGen()
}
}

\bf{Fonction} \MorphismeGen{}{}\\
\Deb{

  $l_{0} =ALEATOIRE(Gamme~saisie~utilisateur)$ \\
  $l_{1} =ALEATOIRE(Gamme~saisie~utilisateur)$ \\
 $ w_{0}= |l_{0}| $\\
 $ w_{1}= |l_{1}|$\\
$w_{0}[0]= [0]  $\\
$w_{1}[0]= [1]  $\\ 
$w_{0}[l_{0}]= [0]  $\\ 
$w_{1}[l_{1}]= [1]  $\\   
\tcc{|| signifie \og ou \fg{}}
\Pour{$i \leftarrow 1$ \bf{\`{a}} $|w_{0}| || | w_{1} |$}{ 
$PAS~ALEATOIRE_{0 || 1}=ALEATOIRE(\leftarrow , \rightarrow ,  \uparrow  , \downarrow)$\\
	 	     
\Si(){$i>1$}{$w_{(0 , 1)}[i]=PAS~ALEATOIRE_{(0 , 1)}$\\} 	 	     
\Si(){$w_{(0 || 1)}[i]==0$ \bf{\&} $w_{(0 || 1)}[i-1]==2$}{$i=i-1$\\}
\Si(){$w_{(0 ||1)}[i]==2$ \bf{\&} $w_{(0 || 1)}[i-1]==0$}{$i=i-1$\\} 	    	 
\Si(){$w_{(0 || 1)}[i]==1$ \bf{\&} $w_{(0 || 1)}[i-1]==3$}{$i=i-1$\\} 	    	 
\Si(){$w_{(0 || 1)}[i]==3$ \bf{\&} $w_{(0 || 1)}[i-1]==1$}{$i=i-1$\\} 	    	 
\Si(){$i==w_{0 || 1}[|w_{0 || 1}|-2]$ \bf{\&} $w_{0 || 1}[|w_{0 || 1}|-2]==2$}{$i=i-1$\\}	  	 
}
$w_{2} = \widehat{w_{0}}$\\
$w_{3} = \widehat{w_{1}}$\\
$\phi= w_{0} \cdot w_{1} \cdot w_{2} \cdot w_{3}$\\
$Boolean ferm\acute{e}+auto-\acute{e}vitant=ferm\acute{e}+auto-\acute{e}vitant(\phi)$\\
\Si(){$ ferm\acute{e}+auto-\acute{e}vitant== \bf{vrai}$}{$\bf{Afficher} \phi $\\} 
}
\caption{Générateur pseudo aléatoire de polyominos Composés}\label{alg:gpara} 


\end{algorithm}
\DecMargin{1em}

\selectlanguage{french}%
\newpage
\begin{figure}[tbph]
\begin{center}
\includegraphics[scale=0.55]{Logos/gparafigure}
\caption{GPara. Banc d'essai disponible dans l'archive électronique jointe comme matériel complémentaire.}
\end{center}
\end{figure}

\section{Résultats empiriques }

À la figure \ref{descriptionpp}, on peut y lire le temps d'exécution des algorithmes de factorisation sur des polyominos de longueur de mot de contour ($\ell<50$):

\begin{figure}[tbph]
\includegraphics[scale=0.8]{Logos/tfapremier}
\caption{Temps de factorisation (polyominos premiers) }
\label{descriptionpp}
\end{figure}

\begin{enumerate}
  \item Pour l'algorithme naïf, on note une fonction à croissance lente et soutenue.
  \item Pour l'algorithme amélioré, on note une fonction croissante avec une phase de croissance rapide, $28 < \ell < 40$ et une phase de plateau, $\ell < 25$. 
\end{enumerate}

\begin{table}[tbph]
\begin{tabular}{|c|c|c|c|c|}
\hline 
{\scriptsize{$l$}} & {\scriptsize{Algorithme naïf (en $ms$)}} & {\scriptsize{Ordre de complexité local ($l^a$)}} & {\scriptsize{Algorithme amélioré (en $ms$)}} & {\scriptsize{Ordre de complexité local ($l^a$)}}\tabularnewline
\hline 
\hline 
{\footnotesize{20}} & {\footnotesize{230}} & {\footnotesize{2.7}} & {\footnotesize{25}} & {\footnotesize{0.6}}\tabularnewline
\hline 
{\footnotesize{30}} & {\footnotesize{313}} & {\footnotesize{1.54}} & {\footnotesize{93}} & {\footnotesize{0.36}}\tabularnewline
\hline 
{\footnotesize{40}} & {\footnotesize{375}} & {\footnotesize{1.41}} & {\footnotesize{300}} & {\footnotesize{0.27}}\tabularnewline
\hline 
{\footnotesize{45}} & {\footnotesize{386}} & {\footnotesize{1.83}} & {\footnotesize{321}} & {\footnotesize{0.29}}\tabularnewline
\hline 
\end{tabular}
\caption{Ordre de complexité empirique local ( polyominos premiers)}
\label{tabp}
\end{table}

Au tableau \ref{tabp}, on décrit l'ordre de complexité empirique local des polyominos de longueur de mot de contour ($\ell<50$):

\begin{enumerate}
  \item On voit clairement que l'algorithme naïf   présente un ordre linéaire de la croissance ne suivant pas une règle de puissance. 
  \item On voit clairement que l'algorithme amélioré  présente un ordre linéaire de la croissance ne suivant pas une règle de puissance. 
  \item Localement, l'ordre de complexité de l'algorithme amélioré est plus faible que celui de l'algorithme naïf.
\end{enumerate}

\begin{figure}[tbph]
\includegraphics[scale=0.8]{Logos/compo}
\caption{Temps de factorisation (polyominos composés)}
\label{nnnnnn}
\end{figure}

À la figure \ref{nnnnnn}), on peut lire les temps d'éxécution de tests de primalité pour des polyominos composés de longueur $\ell < 900$~:

\begin{enumerate}
  \item Pour l'algorithme naïf, on observe des croissances rapides, $0 < \ell < 100$, $260 < \ell <420$, et des phases de plateau, $180 < \ell < 270$, $430 < \ell < 900$.
  \item Pour l'algorithme amélioré, on note une fonction croissante  avec des phases de croissance rapide, $170 < \ell < 260$, $420 < \ell < 900$, et des phases de plateau, $10 < \ell < 280$, $260 < \ell < 620$.
\end{enumerate}

\begin{table}[tbph]
\begin{tabular}{|c|c|c|c|c|}
\hline 
{\scriptsize{$L$}} & {\scriptsize{Algorithme naïf (en $ms$)}} & {\scriptsize{Ordre de complexité local ($L^a$)}} & {\scriptsize{Algorithme amélioré (en $ms$)}} & {\scriptsize{Ordre de complexité local ($L^a$)}}\tabularnewline
\hline 
\hline 
{\footnotesize{20}} & {\footnotesize{104}} & {\footnotesize{0.3}} & {\footnotesize{13}} & {\footnotesize{0.15}}\tabularnewline
\hline 
{\footnotesize{40}} & {\footnotesize{156}} & {\footnotesize{1.7}} & {\footnotesize{16}} & {\footnotesize{0.29}}\tabularnewline
\hline 
{\footnotesize{60}} & {\footnotesize{218}} & {\footnotesize{1.21}} & {\footnotesize{16}} & {\footnotesize{0.01}}\tabularnewline
\hline 
{\footnotesize{100}} & {\footnotesize{1060}} & {\footnotesize{0.32}} & {\footnotesize{412}} & {\footnotesize{0.15}}\tabularnewline
\hline 
{\footnotesize{170}} & {\footnotesize{6098 }} & {\footnotesize{0.3}} & {\footnotesize{494}} & {\footnotesize{2.92}}\tabularnewline
\hline 
{\footnotesize{268}} & {\footnotesize{27653}} & {\footnotesize{0.3}} & {\footnotesize{1143}} & {\footnotesize{0.54}}\tabularnewline
\hline 
{\footnotesize{420}} & {\footnotesize{32389}} & {\footnotesize{2.86}} & {\footnotesize{6320}} & {\footnotesize{0.26}}\tabularnewline
\hline 
{\footnotesize{618}} & {\footnotesize{73530}} & {\footnotesize{0.47}} & {\footnotesize{11754}} & {\footnotesize{0.62}}\tabularnewline
\hline 
{\footnotesize{896}} & {\footnotesize{155538}} & {\footnotesize{0.49}} & {\footnotesize{51630}} & {\footnotesize{0.25}}\tabularnewline
\hline 
\end{tabular}

\caption{Ordre de complexité empirique local (polyominos composés)}
\label{aaaa}
\end{table}

Au tableau \ref{aaaa}, on trouve l'ordre de complexité empirique local des polyominos composés  de longueur de mot de contour $\ell < 900$~:
\begin{enumerate}
  \item On voit clairement que l'algorithme naïf présente un ordre linéaire de la croissance ne suivant pas une règle de puissance. 
  \item On voit clairement que l'algorithme amélioré  présente un ordre linéaire de la croissance ne suivant pas une règle de puissance. 
  \item Localement, l'ordre de complexité de l'algorithme amélioré est plus faible que celui de l'algorithme naïf.
\end{enumerate}

\section{Analyse des résultats empiriques }

On note les caractéristiques et conclusions  suivantes sur le comportement local des algorithmes de factorisation pour des mots de contour:
\begin{enumerate}
  \item Pour des polyominos premiers et composés, dans les deux cas, on a des fonctions linéaires globalement croissantes en fonction de la longueur du mot de contour. 
  \item Pour des polyominos premiers ou composés, l'algorithme amélioré est beaucoup plus efficace en temps de factorisation que l'algorithme naïf.
  \item L'indicateur $a$ dénote d'une meilleure performance de l'algorithme de factorisation amélioré par rapport à l'algorithme de factorisation naïf. 
  \item Pour des polyominos premiers ou composés, l'algorithme naïf et amélioré ne suivent pas de règle de puissance empirique locale. On en déduit que la la borne empirique n'est pas atteinte puisque $a$ varie selon les données d'entrées. On en conclut que les données générées sont insuffisantes pour atteindre la borne empirique. 
\end{enumerate}

\section{Conclusion }

En clair, nous savons que la borne empirique pour nos deux algorithmes de factorisation est loin d'être atteinte. Il nous faut beaucoup plus de données en entrée. Il est probable également que les algorithmes de génération génèrent trop rarement les cas les plus défavorables. Au niveau empirique local, on n'a pas pu atteindre la borne empirique mais clairement, la comparaison empirique locale nous montre que l'algorithme amélioré est plus performant en temps d'exécution, ce qui est cohérent avec les bornes théoriques démontrées.

L'explication de la performance empirique locale de l'algorithme amélioré est la suivante :
\begin{enumerate}
  \item  En choisissant chaque occurrence de $\0$ en $w$ pour construire $\varphi(\0)$, soit  $k_{0}$ le premier conjugé de $w$ commençant par $\0$ et construisant $d$ images pour $\varphi(\0)$.
  \item  Soit  $k_{0}$ determiné en choisissant chaque occurrence de $\1$  en $w$ pour construire $\varphi(\1)$, soit  $k_{1}$ le premier conjugé de $w$ commençant par $\1$ et construisant $d$
images pour $\varphi(\1)$.
  \item  Soit $N$ le nombre de tentatives globales pour construire les   $k$ conjugués  et leur images de $w$ commençant par $\0$ et $\1$. 
\end{enumerate}
La fonction $skip()$, avec comme séquence d'entrée le mot de contour $w$, retourne une séquence qui ignore $N$ lettres de la séquence sous-jacente.
Pour que l'algorithme soit le plus rapide possible, supposons que les quatre images de $\varphi$ sont choisies,  la probabilité $prob(k_{0,1})$ que le premier essai factorise $P$ et que $u$ et $v$ surviennent de façon contiguë est très forte puisque $N \leq  skip(|w|,k_{0,1})$ pour un certain $\ell < 60$. Dès lors, on obtient un temps global constant de factorisation $\textbf{t}$ des polyominos composés pour des longueurs de contour $\ell < 60$. 

En conclusion, l'évaluation expérimentale nous a  permis de mieux comprendre certaines caractéristiques spécifiques du fonctionnement des algorithmes de factorisation des polyominos au niveau empirique que l'analyse théorique ne peut déceler.


